package main

import (
	LRU "LRUCache"
	"WAL"
	LSM "lsm"
	MT "memTable"
	"menu"
	"strconv"
	"strings"
	TB "tokenBucket"
	"utils"
)

const ConfigPath = "config.yaml"

func main() {

	WAL.CheckHashFunction()
	config := utils.ReadConfig(ConfigPath)
	config.SaveConfig(ConfigPath)

	numMemtables := config.MemTable.NumMemTables
	size := config.MemTable.MemTableSize
	structure := config.MemTable.Structure

	var mth MT.MemTablesHandler
	switch structure {
	case "SkipList":
		mth = MT.NewMemTablesHandlerSkipList(numMemtables, size)
		break
	case "HashMap":
		mth = MT.NewMemTablesHandlerHashMap(numMemtables, size)
		break
	case "B-Tree":
		mth = MT.NewMemTablesHandlerBTree(numMemtables, size)
		break
	default:
		mth = MT.NewMemTablesHandlerSkipList(numMemtables, size)
		break
	}

	tb, _ := TB.DeserializeTokenBucket("tokenBucket/tokenBucket/tb.db")
	tb.SetCapacity(config.TokenBucket.Capacity)
	tb.SetRefillPeriod(config.TokenBucket.RefillPeriodMinutes)

	cache := LRU.NewLRUCache(config.Cache.CacheSize)
	lsm, _ := LSM.DeserializeLSM("LSM/lsm/lsm.db")
	p, s := mth.LoadAllFromWal(*config, lsm.MaxTable(), lsm)
	needMerging := lsm.CheckForMerge(0)
	if needMerging {
		lsm.MergeLevel(*config)
		lsm.SerializeLSM("LSM/lsm/lsm.db")
	}
	s = strings.Split(strings.Split(s, "_")[1], ".")[0]
	seg, _ := strconv.Atoi(s)
	run := true
	for run {
		run = menu.StartingMenu(*config, &mth, lsm, cache, tb, p, seg)
	}

	//menu.StartingMenu(*config, *mth, lsm, cache)

	//fmt.Println(config.Cache.CacheSize)

	// config.BloomFilter.SetFalsePositive(0.05)
	// config.LSMTree.SetNumSSTables(4)
	// config.LSMTree.SetLevels(2)
	// fmt.Println(config.Cache.CacheSize)
	// config.SaveConfig(ConfigPath)
	// config.LSMTree.SetLevels(3)
	// config.Cache.SetSize(80)
	//config.SaveConfig(ConfigPath)

	// fmt.Println(config.BloomFilter.FalsePositive)

	// hash := WAL.CreateHashFunction()
	// fmt.Println(hash)
	// i, _ := WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc1", "Vrednost1", false, i, 70)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc2", "Vrednost2", false, i, 70)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc3", "Vrednost3", false, i, 70)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc4", "Vrednost4", false, i, 70)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc5", "Vrednost5", false, i, 70)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc6", "Vrednost6", false, i, 70)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc7", "Vrednost7", false, i, 70)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc8", "Vrednost8", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc9", "Vrednost9", true, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc10", "Vrednost10", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc11", "Vrednost11", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc12", "Vrednost12", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc13", "Vrednost13", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc14", "Vrednost14", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc15", "Vrednost15", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc16", "Vrednost16", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc17", "Vrednost17", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc18", "Vrednost18", true, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc19", "Vrednost19", false, i, 100)
	// i, _ = WAL.CountSegments("WAL/Segments")
	// WAL.WriteRecord("Kljuc20", "Vrednost20", false, i, 100)

	// lsm := LSM.NewLSM(config.LSMTree.MaxLevels, config.LSMTree.NumSSTables)
	// n := config.MemTable.NumMemTables
	// size := config.MemTable.MemTableSize
	// var mthsl MT.MemTablesHandlerSkipList
	// mth := mthsl.NewMemTablesHandler(n, size)

	// tableId := lsm.MaxTable()
	// mth.LoadAllFromWal(*config, tableId, lsm)

	// nedMerging := lsm.CheckForMerge(0)e
	// if needMerging {
	// 	lsm.MergeLevel(*config)
	// }
	// lsm.SerializeLSM("LSM/lsm/lsm.db")
	// fmt.Println(lsm)

	// sstable := SST.NewSSTableLoad(0, 2, 4, 0.05)
	// sstable.Summary.LoadSummary()
	// key := "Kljuc3"
	// offsetIndex, end := sstable.Summary.Find(key)
	// fmt.Println(offsetIndex, end)
	// offsetData := sstable.Index.FindKey(key, offsetIndex, end)
	// fmt.Println(offsetData)
	// value, check := sstable.Data.LoadData(offsetData)
	// fmt.Println("Vrednost:", value.Value)
	// fmt.Println(check)
}
