package MerkleStablo

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"os"
)

type MerkleTreeNode struct {
	Data  string
	Left  *MerkleTreeNode
	Right *MerkleTreeNode
}

func NewMerkleTreeNode(Data string) *MerkleTreeNode {
	return &MerkleTreeNode{
		Data:  Data,
		Left:  nil,
		Right: nil,
	}
}

type MerkleTree struct {
	Root *MerkleTreeNode
}

func NewMerkleTree(dataSet []string) *MerkleTree {

	//sha256.Sum256 daje [215 254 7 160 103 155 126 6 93 56 122 195 129 76 213 119 240 85 157 95 184 152 103 30 237 206 88 1 164 254 226 106]
	//za svaki element iz seta, a hex.EncodeToString(hash[:]) to pretvara u string od 64 karaktera koji izgleda ovako
	//cd9fb1e148ccd8442e5aa74904cc73bf6fb54d1d54d333bd596aa9bb4bb4e961 i to je vrednost parametra data u TreeNod-u
	var treeLeafs []MerkleTreeNode
	for _, data := range dataSet {
		hash := sha256.Sum256([]byte(data))
		treeNode := NewMerkleTreeNode(hex.EncodeToString(hash[:]))
		treeLeafs = append(treeLeafs, *treeNode) //u hashDataSet nalaze se svi listovi(data blok)
	}

	for len(treeLeafs) > 1 {
		var nextLevelNodes []MerkleTreeNode //lista u koju se smestaju novokreirani elementi
		for i := 0; i < len(treeLeafs); i += 2 {
			if len(treeLeafs)%2 != 0 { //u slucaju da je broj elemenata neparan dodaje se neutralni element ciji data = "" kako bi se svi cvorovi mogli spojiti
				NeutralNode := NewMerkleTreeNode("")
				treeLeafs = append(treeLeafs, *NeutralNode)
			}
			newNodeData := treeLeafs[i].Data + treeLeafs[i+1].Data //vrednost novog cvora za visi nivo
			hashNewNodeData := sha256.Sum256([]byte(newNodeData))  //hesiranje te vrednosti

			newTreeNode := NewMerkleTreeNode(hex.EncodeToString(hashNewNodeData[:])) //taj hes se pretvori u string i pravi se novi cvor
			newTreeNode.Left = &treeLeafs[i]                                         //parametri left i right se podesavaju
			newTreeNode.Right = &treeLeafs[i+1]

			nextLevelNodes = append(nextLevelNodes, *newTreeNode) //u nextLevelNodes se dodaju svi cvorovi koji ce biti u sledecem nivou

		}
		treeLeafs = nextLevelNodes //sve se ponavlja sa novom listom koju smo upravo napravili
	}
	root := &treeLeafs[0]
	return &MerkleTree{Root: root} //vraca objekat MerkleTree

}

func (mt *MerkleTree) Contains(data string) bool {
	//ova funkcija ocekuje vrednost elementa ciji hes se dalje proverava da li je u tree-u odnoso jedan od listova
	//da li je element neki od listova
	hash := sha256.Sum256([]byte(data))
	hashStr := hex.EncodeToString(hash[:])
	return mt.ContainsLeaf(mt.Root, hashStr)
}

func (mt *MerkleTree) ContainsLeaf(node *MerkleTreeNode, data string) bool {
	//ContainsLeaf se poziva iz Contains i rekurzijom dolazi do toga da li je element u tree-u
	if node == nil {
		return false
	}
	if node.Left == nil && node.Right == nil && node.Data == data {
		return true
	}
	return mt.ContainsLeaf(node.Left, data) || mt.ContainsLeaf(node.Right, data)
}

func (mt *MerkleTree) DamagedElements(dataSet []string) []string {
	//funkcija vraca sve elemente koji su osteceni, ocekivalo bi se da je dataSet takodje set podataka
	//od kojih je kreiran tree nad kojim se ova funkcija poziva u suprotnom vecina elemenata ce biti osteceni ako ne i svi
	var damagedElements []string
	for _, data := range dataSet {
		if mt.Contains(data) == false {
			damagedElements = append(damagedElements, data)
		}
	}
	return damagedElements
}

func (mt *MerkleTree) CheckMerkleTree(dataSet []string) bool {
	//od prosledjenog skupa podataka pravi novi merkle tree i poredi vrednost parametra data korenskog cvora
	//sa vec postojecim merkle tree-em za neki(isti) skup podataka
	merkleTreeDataSet := NewMerkleTree(dataSet)
	return mt.Root.Data == merkleTreeDataSet.Root.Data

}

func (mt *MerkleTree) SerializeMerkleTree(filePath string) error {
	//zadati merkle tree zapisuje binarno u fajl sa zadatom putanjom
	fajl, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("Greška prilikom otvaranja fajla!")
		return err
	}
	defer fajl.Close()

	data, err := json.Marshal(mt)
	if err != nil {
		fmt.Println("Greška prilikom serijalizacije:", err)
		return err
	}

	_, err = fajl.Write(data)
	if err != nil {
		fmt.Println("Greška prilikom pisanja u fajl:", err)
	}

	fmt.Println("MerkleTree objekat uspešno serijalizovan u fajl sa ekstenzijom")
	return nil
}

func DeserializeMerkleTree(filePath string) (*MerkleTree, error) {
	//od zadatog fajla ocekuje se da sadzi tree u binarnom formatu zatim otvara zadati fajl i dekodira sadrzaj u merkle tree objekat

	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var mt MerkleTree
	err = json.Unmarshal(data, &mt)
	if err != nil {
		return nil, err
	}

	return &mt, nil
}

func CheckTwoDataSet(dataSet1 []string, dataSet2 []string) bool {
	//poredjene dva skupa podataka tako sto se oba smeste u dva merkle tree-a a zatim im se provere koreni
	merkleTreeDataSet1 := NewMerkleTree(dataSet1)
	merkleTreeDataSet2 := NewMerkleTree(dataSet2)
	return merkleTreeDataSet1.Root.Data == merkleTreeDataSet2.Root.Data

}
