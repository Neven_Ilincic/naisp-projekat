package lsm

import (
	EL "element"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	SST "sstable"
	"strconv"
	CONF "utils"
)

type LSM struct {
	MaxLevel         int
	MaxTablesOnLevel int
	Stats            []*[]int //Stats[0] prvi level, Stats[1] drugi level ,...
}

func NewLSM(maxLevel, maxTablesOnLevel int) *LSM {
	stats := make([]*[]int, maxLevel)
	for i := range stats {
		level := make([]int, maxTablesOnLevel)
		stats[i] = &level
	}
	return &LSM{
		MaxLevel:         maxLevel,
		MaxTablesOnLevel: maxTablesOnLevel,
		Stats:            stats,
	}
}

func (lsm *LSM) CheckForMerge(level int) bool {
	//true ako treba da se spaja nivo
	firstLevel := *lsm.Stats[level]

	if firstLevel[lsm.MaxTablesOnLevel-1] == 0 {
		return false
	} else {
		return true
	}
}

func (lsm *LSM) AddSSTable(num, level int) {
	//dodaje id sstabele u prvi nivo
	firstLevel := *lsm.Stats[level]
	for k := range firstLevel {
		if firstLevel[k] == 0 {
			firstLevel[k] = num
			sort.Sort(sort.Reverse(sort.IntSlice(firstLevel)))
			*(lsm.Stats[level]) = firstLevel
			return
		}
	}

}

func (lsm *LSM) MaxTable() int {
	max := 0
	for i := range lsm.Stats {
		for j := range *(lsm.Stats[i]) {
			if (*(lsm.Stats[i]))[j] > max {
				max = (*(lsm.Stats[i]))[j]
			}
		}
	}
	return max + 1
}

func (lsm *LSM) MergeTwoSSTable(table1, table2, level int, config CONF.Config) {
	indexTable1Path := "tables/usertable-" + strconv.Itoa(table1) + "-index.db"
	dataTable1Path := "tables/usertable-" + strconv.Itoa(table1) + "-data.db"
	indexTable2Path := "tables/usertable-" + strconv.Itoa(table2) + "-index.db"
	dataTable2Path := "tables/usertable-" + strconv.Itoa(table2) + "-data.db"

	indexFileTable1, err := os.OpenFile(indexTable1Path, os.O_RDONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("Error opening indexFile:", err)
		return
	}
	dataFileTable1, err := os.OpenFile(dataTable1Path, os.O_RDONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("Error opening indexFile:", err)
		return
	}
	indexFileTable2, err := os.OpenFile(indexTable2Path, os.O_RDONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("Error opening indexFile:", err)
		return
	}
	dataFileTable2, err := os.OpenFile(dataTable2Path, os.O_RDONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("Error opening indexFile:", err)
		return
	}

	var elements []EL.Element
	dataFileTable1.Seek(4, 1)
	dataFileTable2.Seek(4, 1)

	firstKey, err1 := SST.ReturnOneRecord(indexFileTable1)
	secondKey, err2 := SST.ReturnOneRecord(indexFileTable2)
	firstValue, firstTime, firstTombstone, err := SST.LoadOneELement(dataFileTable1)
	secondValue, secondTime, secondTombstone, err := SST.LoadOneELement(dataFileTable2)

	element1 := EL.NewElement(firstTombstone, firstTime, firstKey, firstValue)
	element2 := EL.NewElement(secondTombstone, secondTime, secondKey, secondValue)

	firstEnd := false
	secondEnd := false

	for {
		//ako je samo prvi fajl dosao do kraja
		if firstEnd && !secondEnd {
			if !element2.Tumbstone {
				elements = append(elements, *element2)
			}
			indexFileTable2.Seek(8, 1)
			dataFileTable2.Seek(4, 1)
			secondKey, err2 = SST.ReturnOneRecord(indexFileTable2)
			if err2 != nil {
				secondEnd = true
				continue
			}
			secondValue, secondTime, secondTombstone, err = SST.LoadOneELement(dataFileTable2)
			element2 = EL.NewElement(secondTombstone, secondTime, secondKey, secondValue)
			continue
			//ako je samo drugi fajl dosao do kraja
		} else if !firstEnd && secondEnd {
			if !element1.Tumbstone {
				elements = append(elements, *element1)
			}
			indexFileTable1.Seek(8, 1)
			dataFileTable1.Seek(4, 1)
			firstKey, err1 = SST.ReturnOneRecord(indexFileTable1)
			if err1 != nil {
				firstEnd = true
				continue
			}
			firstValue, firstTime, firstTombstone, err = SST.LoadOneELement(dataFileTable1)
			element1 = EL.NewElement(firstTombstone, firstTime, firstKey, firstValue)
			continue
			//ako su oba do kraja izlaz
		} else if firstEnd && secondEnd {
			break
		}

		//ako oba imaju jos elemenata
		if element1.Key < element2.Key {
			//prvi kljuc manji
			if !element1.Tumbstone {
				elements = append(elements, *element1)
			}
			indexFileTable1.Seek(8, 1)
			dataFileTable1.Seek(4, 1)
			firstKey, err1 = SST.ReturnOneRecord(indexFileTable1)
			if err1 != nil {
				firstEnd = true
				continue
			}
			firstValue, firstTime, firstTombstone, err = SST.LoadOneELement(dataFileTable1)
			element1 = EL.NewElement(firstTombstone, firstTime, firstKey, firstValue)
			continue

		} else if element2.Key < element1.Key {
			//drugi kljuc manji
			if !element2.Tumbstone {
				elements = append(elements, *element2)
			}
			indexFileTable2.Seek(8, 1)
			dataFileTable2.Seek(4, 1)
			secondKey, err2 = SST.ReturnOneRecord(indexFileTable2)
			if err2 != nil {
				secondEnd = true
				continue
			}
			secondValue, secondTime, secondTombstone, err = SST.LoadOneELement(dataFileTable2)
			element2 = EL.NewElement(secondTombstone, secondTime, secondKey, secondValue)
			continue
		} else {
			//isti kljucevi (gleda se vreme)
			if element1.Timestamp.After(element2.Timestamp) {
				if !element1.Tumbstone {
					elements = append(elements, *element1)
				}
				indexFileTable1.Seek(8, 1)
				dataFileTable1.Seek(4, 1)
				indexFileTable2.Seek(8, 1)
				dataFileTable2.Seek(4, 1)
				firstKey, err1 = SST.ReturnOneRecord(indexFileTable1)
				secondKey, err2 = SST.ReturnOneRecord(indexFileTable2)
				if err1 != nil || err2 != nil {
					if err1 != nil {
						firstEnd = true
					}
					if err2 != nil {
						secondEnd = true
					}
					continue
				}

				firstValue, firstTime, firstTombstone, err = SST.LoadOneELement(dataFileTable1)
				element1 = EL.NewElement(firstTombstone, firstTime, firstKey, firstValue)

				secondValue, secondTime, secondTombstone, err = SST.LoadOneELement(dataFileTable2)
				element2 = EL.NewElement(secondTombstone, secondTime, secondKey, secondValue)
				continue

			} else {
				if !element2.Tumbstone {
					elements = append(elements, *element2)
				}
				indexFileTable1.Seek(8, 1)
				dataFileTable1.Seek(4, 1)
				indexFileTable2.Seek(8, 1)
				dataFileTable2.Seek(4, 1)
				firstKey, err1 = SST.ReturnOneRecord(indexFileTable1)
				secondKey, err2 = SST.ReturnOneRecord(indexFileTable2)
				if err1 != nil || err2 != nil {
					if err1 != nil {
						firstEnd = true
					}
					if err2 != nil {
						secondEnd = true
					}
					continue
				}

				firstValue, firstTime, firstTombstone, err = SST.LoadOneELement(dataFileTable1)
				element1 = EL.NewElement(firstTombstone, firstTime, firstKey, firstValue)

				secondValue, secondTime, secondTombstone, err = SST.LoadOneELement(dataFileTable2)
				element2 = EL.NewElement(secondTombstone, secondTime, secondKey, secondValue)
				continue
			}
		}
	}
	//brisanje
	indexFileTable1.Close()
	dataFileTable1.Close()
	indexFileTable2.Close()
	dataFileTable2.Close()

	err = os.Remove("tables/usertable-" + strconv.Itoa(table1) + "-index.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table1) + "-data.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table1) + "-filter.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table1) + "-metadata.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table1) + "-summarry.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table2) + "-index.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table2) + "-data.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table2) + "-filter.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table2) + "-metadata.db")
	err = os.Remove("tables/usertable-" + strconv.Itoa(table2) + "-summarry.db")
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	//brisanje tabela iz lsma
	var new int
	max := lsm.MaxTable() //id za novu tabelu
	lsmLevel := *lsm.Stats[level-1]
	for k := range lsmLevel {
		if lsmLevel[k] == table1 || lsmLevel[k] == table2 {
			lsmLevel[k] = 0
			new = k //zad novu
		}
	}

	sstable := SST.NewSSTable(max, level, len(elements), 0.05, elements)
	sstable.WriteTable(elements, config.Summary.Thinning, max)

	//nova se upisuje na isti nivo(ako ostane jedna ta se prebacuje u visi)
	lsmLevel[new] = max
	sort.Sort(sort.Reverse(sort.IntSlice(lsmLevel)))
	*(lsm.Stats[level-1]) = lsmLevel
}

func (lsm *LSM) MergeLevel(config CONF.Config) {
	n := lsm.MaxTablesOnLevel
	level := 1
	var numPairs int
	for {
		if n%2 != 0 {
			numPairs = (n - 1) / 2
		} else {
			numPairs = n / 2
		}

		listOfPairs := make([][]int, numPairs)
		for i := range listOfPairs {
			pair := make([]int, 2)
			listOfPairs[i] = pair
		}

		for i := 0; i < numPairs; i += 1 {
			listOfPairs[i][0] = (*(lsm.Stats[level-1]))[2*i]
			listOfPairs[i][1] = (*(lsm.Stats[level-1]))[2*i+1]
		}

		for k := range listOfPairs {
			lsm.MergeTwoSSTable(listOfPairs[k][0], listOfPairs[k][1], level, config)
		}

		if n%2 != 0 {
			n = numPairs + 1
		} else {
			n = numPairs
		}
		if n == 1 {
			//id nove tabele
			idNewTable := (*(lsm.Stats[level-1]))[0]
			//brisanje u starom nivou
			currentLevel := *lsm.Stats[level-1]
			currentLevel[0] = 0
			*(lsm.Stats[level-1]) = currentLevel

			lsm.AddSSTable(idNewTable, level)
			if lsm.CheckForMerge(level) {
				level += 1
				n = lsm.MaxTablesOnLevel
				continue
			} else {
				break
			}

		}

	}

}

func (lsm *LSM) SerializeLSM(filePath string) error {

	data, err := json.MarshalIndent(lsm, "", "  ")
	if err != nil {
		return err
	}

	err = os.WriteFile(filePath, data, 0644)
	if err != nil {
		return err
	}

	fmt.Println("LSM struktura uspešno serijalizovana.")
	return nil
}

func DeserializeLSM(filePath string) (*LSM, error) {

	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var lsm LSM
	err = json.Unmarshal(data, &lsm)
	if err != nil {
		return nil, err
	}

	return &lsm, nil
}
