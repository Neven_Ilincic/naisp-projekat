package simhash

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"os"
	"strings"
	"unicode"
)

type SimHash struct {
	HashSize uint
	Hashes   []byte
	Weights  map[string]uint
}

func NewSimHash(hsize uint) *SimHash {
	return &SimHash{
		HashSize: hsize,
		Hashes:   make([]byte, hsize),
		Weights:  make(map[string]uint),
	}
}

func HashString(input string) uint64 {
	hashx := fnv.New64a()
	_, err := hashx.Write([]byte(input))
	if err != nil {
		return 0
	}
	return hashx.Sum64()
}

func LoadData(putanja string) string {
	content, err := os.ReadFile(putanja)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return "Error"
	}

	text := string(content)
	return text
}

func (sh *SimHash) PopulateMap(text string) {
	text2 := strings.ToLower(text)
	words := strings.Fields(text2)

	for _, word := range words {
		wordx := strings.TrimRightFunc(word, unicode.IsPunct)
		value, exists := sh.Weights[wordx]
		if exists {
			sh.Weights[wordx] = value + 1
		} else {
			sh.Weights[wordx] = 1
		}
	}
}

func GetHashAsString(data []byte) string {
	hash := md5.Sum(data)
	res := ""
	for _, b := range hash {
		res = fmt.Sprintf("%s%b", res, b)
	}
	return res
}

func Bits(num uint64, k int) []int {
	bits := make([]int, k)

	for i := 0; i < k; i++ {
		bits[i] = int((num >> uint(i)) & 1)
	}

	for i, j := 0, k-1; i < j; i, j = i+1, j-1 {
		bits[i], bits[j] = bits[j], bits[i]
	}

	return bits
}

func (sh *SimHash) SetHashes(text string) {
	sh.PopulateMap(text)
	arr := make([]int, sh.HashSize)
	for key, value := range sh.Weights {
		vrednost := HashString(key)
		bits := Bits(vrednost, 12)
		for i := 0; i < len(bits); i++ {
			if bits[i] == 1 {
				arr[i] += int(value)
			} else {
				arr[i] -= int(value)
			}
		}
	}
	for j := 0; j < int(sh.HashSize); j++ {
		if arr[j] >= 0 {
			sh.Hashes[j] = 1
		} else {
			sh.Hashes[j] = 0
		}
	}
}

func (sh *SimHash) SetHashesFile(putanja string) {
	text := LoadData(putanja)
	sh.SetHashes(text)
}

func (sh *SimHash) HammingDistance(sh2 *SimHash) uint {
	if sh.HashSize != sh2.HashSize {
		panic("Hashes must have the same length for XOR operation.")
	}

	xor := make([]byte, sh.HashSize)
	var distance uint = 0

	for i := 0; i < int(sh.HashSize); i++ {
		xor[i] = sh.Hashes[i] ^ sh2.Hashes[i]
		distance += uint(xor[i])
	}

	return distance
}

func (sh *SimHash) SerializeSimHash(path string) error {
	file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer file.Close()

	data, err := json.Marshal(sh)
	if err != nil {
		return err
	}

	_, err = file.Write(data)
	if err != nil {
		return err
	}

	return nil
}

func DeserializeSimHash(filePath string) (*SimHash, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	var simHash SimHash
	if err := decoder.Decode(&simHash); err != nil {
		return nil, err
	}

	return &simHash, nil
}

// func main() {
// 	/*
// 		sh1 := NewSimHash(16)
// 		sh1.SetHashesFile("tekst1.txt")
// 		fmt.Println("SH1 hashes:", sh1.Hashes)

// 		sh2 := NewSimHash(16)
// 		sh2.SetHashesFile("tekst2.txt")
// 		fmt.Println("SH2 hashes:", sh2.Hashes)

// 		fmt.Println("Hamming distance SH1 to SH1:", sh1.HammingDistance(sh1))
// 		fmt.Println("Hamming distance SH1 to SH2:", sh1.HammingDistance(sh2))
// 		fmt.Println("Hamming distance SH2 to SH1:", sh2.HammingDistance(sh1))
// 		fmt.Println("Hamming distance SH2 to SH2:", sh2.HammingDistance(sh2))

// 		sh1.Serialization("C:\\Users\\Korisnik\\Documents\\serijalizovano.gob")
// 		sh := Deserialization("C:\\Users\\Korisnik\\Documents\\serijalizovano.gob")
// 		fmt.Println("SH  hashes:", sh.Hashes)
// 	*/
// }
