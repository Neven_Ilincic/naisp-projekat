package memTable

import (
	"WAL"
	LSM "lsm"
	"os"
	"sstable"
	"strconv"
	"time"
	"utils"
)

type MemTablesHandlerSkipList struct {
	MemTables        map[int]*MemTableSkipList
	NumMemTables     int
	MemTablesMaxSize int
}

func NewMemTablesHandlerSkipList(numMemTables int, memTablesMaxSize int) *MemTablesHandlerSkipList {
	memTables := make(map[int]*MemTableSkipList)
	for i := 0; i < numMemTables; i += 1 {
		var skipListTable MemTableSkipList
		memTable := skipListTable.NewMemTable(memTablesMaxSize)
		memTables[i] = memTable
	}
	return &MemTablesHandlerSkipList{
		MemTables:        memTables,
		NumMemTables:     numMemTables,
		MemTablesMaxSize: memTablesMaxSize,
	}
}

func (mth *MemTablesHandlerSkipList) IsFullAllTables() bool {
	for _, memTable := range mth.MemTables {
		if !memTable.IsFull() {
			return false
		}
	}
	return true
}

func (mth *MemTablesHandlerSkipList) FindFirstEmpty() int {
	i := 0
	for key := 0; key < len(mth.MemTables); key += 1 {
		currentSize := mth.MemTables[key].CurrentSize
		maxSize := mth.MemTables[key].MaxSize
		if currentSize < maxSize {
			return i
		}
		i += 1
	}
	return i
}

func (mth *MemTablesHandlerSkipList) FindIndexContains(key string) (int, bool) {
	i := 0
	for k := 0; k < len(mth.MemTables); k += 1 {
		skipListNode := mth.MemTables[k].SkipList.Search(key)
		if skipListNode != nil {
			return i, true
		}
		i += 1
	}
	return -1, false
}

func (mth *MemTablesHandlerSkipList) EmptyFirst(config utils.Config, segmentSize, gen int, lsm *LSM.LSM) int {
	if !mth.IsFullAllTables() {
		return 0
	}
	numExisting := mth.MemTables[0].GetNumExisting()
	numDeleted := mth.MemTables[0].GetNumDeleted()
	numChanged := mth.MemTables[0].GetNumChanged()
	hashFunction, _ := WAL.Deserialization("WAL/hesFunkcija.db")
	maxSegmentSize := segmentSize
	walSize, _ := WAL.CountSegments("WAL/Segments")

	_, _, pointer, _ := WAL.ReadMetaData("WAL/metadata.txt")
	numDeletedSegments, _ := WAL.RemoveRecords(numExisting, numDeleted, numChanged, *hashFunction, maxSegmentSize, walSize, pointer)
	WAL.DeleteMetaData("WAL/metadata.txt", numDeletedSegments)
	memTableToEmpty := mth.MemTables[0]

	for i := 0; i < (mth.NumMemTables - 1); i += 1 {
		mth.MemTables[i] = mth.MemTables[i+1]
	}
	var skipListTable MemTableSkipList
	mth.MemTables[mth.NumMemTables-1] = skipListTable.NewMemTable(mth.MemTablesMaxSize)

	elements := memTableToEmpty.SkipList.ReturnAll()

	ssTable := sstable.NewSSTable(gen, 1, config.MemTable.MemTableSize, config.BloomFilter.FalsePositive, elements)
	ssTable.WriteTable(elements, config.Summary.Thinning, gen)
	lsm.AddSSTable(gen, 0)
	check := lsm.CheckForMerge(0)
	if check {
		lsm.MergeLevel(config)
	}
	lsm.SerializeLSM("LSM/lsm/lsm.db")

	return numDeletedSegments

}

func (mth *MemTablesHandlerSkipList) LoadAllFromWal(config utils.Config, gen int, lsm *LSM.LSM) (int, string) { // Vraca pocetni segment
	filePath := "WAL/Segments/wal_1.log"
	segment, _, pointer, _ := WAL.ReadMetaData("WAL/metadata.txt")
	WAL.TruncateFile("WAL/metadata.txt")
	hashFunction, _ := WAL.Deserialization("WAL/hesFunkcija.db")
	maxSegmentSize := config.WAL.MaxSegmentSize
	numSegments, _ := WAL.CountSegments("WAL/Segments")
	metaDataPath := "WAL/metadata.txt"
	var startingSegment bool
	pointerToReturn := pointer
	seg := 1
	poi := pointer
	for i := 1; i <= numSegments; i += 1 {

		for {
			if startingSegment == true {
				seg = i
				startingSegment = false
			}

			file, _ := os.Stat(filePath)
			if i == numSegments {
				maxSegmentSize = config.WAL.MaxSegmentSize // Postaviti na segmentSize iz konfiguracionog fajla
			} else {
				maxSegmentSize = int(file.Size())
			}

			bytes, currentPointer, newSegment := WAL.ReturnRecord(filePath, pointer, *hashFunction, maxSegmentSize)
			tumbstone, timestamp, key, value := WAL.ReadRecord(bytes, *hashFunction)
			pointer = currentPointer

			if mth.IsFullAllTables() {
				numDeletedSegments := mth.EmptyFirst(config, maxSegmentSize, gen, lsm)
				gen += 1
				numSegments -= numDeletedSegments
				i -= numDeletedSegments
				seg -= numDeletedSegments
				filePath = "WAL/Segments/wal_" + strconv.Itoa(i) + ".log"
			}
			var index int
			memTableIndex, found := mth.FindIndexContains(key)
			if found {
				index = memTableIndex
			} else {
				index = mth.FindFirstEmpty()
			}

			//BRISANJE
			if tumbstone {
				deleted := mth.MemTables[index].DeleteElement(key)
				if !deleted {
					mth.MemTables[index].AddElement(tumbstone, timestamp, key, value, false)
					mth.MemTables[index].DeleteElement(key)
				}
				mth.MemTables[index].SetNumDeleted()
			} else {
				//DODAVANJE
				if mth.MemTables[index].GetSkipList().Search(key) == nil {
					mth.MemTables[index].AddElement(tumbstone, timestamp, key, value, false)
					mth.MemTables[index].SetNumExisting()
					//IZMENA
				} else {
					mth.MemTables[index].AddElement(tumbstone, timestamp, key, value, true)
					mth.MemTables[index].SetNumChanged()
				}
			}

			if mth.MemTables[index].IsFull() && !found {
				WAL.WriteMetaData(metaDataPath, "memtable_"+strconv.Itoa(index+1), seg, poi)
				poi = pointer
				startingSegment = true

			}

			fi, _ := os.Stat(filePath)
			size := fi.Size()

			if i+1 == numSegments && newSegment {
				filePathCheck := "WAL/Segments/wal_" + strconv.Itoa(i+1) + ".log"
				fi, _ := os.Stat(filePathCheck)
				size := fi.Size()
				if pointer >= int(size) {
					if mth.IsFullAllTables() {
						mth.EmptyFirst(config, maxSegmentSize, gen, lsm)
						gen += 1
					}

					return pointerToReturn, segment
				}
			}
			if i == numSegments && pointer >= int(size) {
				if mth.IsFullAllTables() {
					mth.EmptyFirst(config, maxSegmentSize, gen, lsm)
					gen += 1
				}

				return pointerToReturn, segment
			}
			if newSegment {
				filePath = "WAL/Segments/wal_" + strconv.Itoa(i+1) + ".log"
				break
			}
		}

	}
	return pointerToReturn, segment
}

func (mth *MemTablesHandlerSkipList) GetMemTablesSkipList() map[int]*MemTableSkipList {
	return mth.MemTables
}
func (mth *MemTablesHandlerSkipList) GetMemTablesHashMap() map[int]*MemTableHashMap {
	return nil
}
func (mth *MemTablesHandlerSkipList) GetMemTablesBTree() map[int]*MemTableBTree {
	return nil
}

// Dodavanje u memtabelu
func (mth *MemTablesHandlerSkipList) WriteToMemtable(key string, value string, tombstone bool, timestamp time.Time, config utils.Config, lsm *LSM.LSM, p int) {
	metaDataPath := "WAL/metadata.txt"
	var index int
	memTableIndex, found := mth.FindIndexContains(key)
	if found {
		index = memTableIndex
	} else {
		index = mth.FindFirstEmpty()
	}

	//BRISANJE
	if tombstone {
		deleted := mth.MemTables[index].DeleteElement(key)
		if !deleted {
			mth.MemTables[index].AddElement(tombstone, timestamp, key, value, false)
			mth.MemTables[index].DeleteElement(key)
		}
		if !found {
			mth.MemTables[index].SetNumDeleted()
		} else {
			mth.MemTables[mth.FindFirstEmpty()].SetNumDeleted()
		}

	} else {
		//DODAVANJE
		if mth.MemTables[index].GetSkipList().Search(key) == nil {
			mth.MemTables[index].AddElement(tombstone, timestamp, key, value, false)
			mth.MemTables[index].SetNumExisting()
			//IZMENA
		} else {
			mth.MemTables[index].AddElement(tombstone, timestamp, key, value, true)
			mth.MemTables[mth.FindFirstEmpty()].SetNumChanged()
		}
	}
	if mth.MemTables[index].IsFull() && !found {
		num := 0
		if index > 0 {
			num = mth.MemTables[index-1].GetNumChanged() + mth.MemTables[index-1].GetNumDeleted() + mth.MemTables[index-1].GetNumExisting()
		}
		seg, poi := WAL.FindPointer(num, config)
		if index == 0 {
			poi = p
		}
		WAL.WriteMetaData(metaDataPath, "memtable_"+strconv.Itoa(index+1), seg, poi)
	}
	if mth.IsFullAllTables() {
		mth.EmptyFirst(config, 0, lsm.MaxTable(), lsm)

	}

}
