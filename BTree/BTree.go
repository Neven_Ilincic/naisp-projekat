package BTree

import (
	EL "element"
	"fmt"
	"time"
)

type BTree struct {
	Root        *BTreeNode
	M           int
	MaxSize     int
	CurrentSize int
}

// konstruktor
func NewBTree(node *BTreeNode, m int, maxsize int) *BTree {
	tree := &BTree{
		Root:        node,
		M:           m,
		MaxSize:     maxsize,
		CurrentSize: 0,
	}
	return tree
}

// Pretraga
func (b *BTree) Search(key string) *BTreeNode {
	return b.Root.Search(key)
}

// Pomocna funkcija za predhodnika
func (b *BTree) Predecessor(key string) string {
	node := b.Search(key)
	if node != nil {
		k := 0
		for index := 0; index < len(node.Keys); index++ {
			if node.Keys[index].Key == key {
				k = index
				break
			}
		}
		child := node.Children[k]

		predecessor := child.Keys[len(child.Keys)-1].Key
		return predecessor
	}
	return ""
}

// Pomocna funkcija za sledbenika
func (b *BTree) Successor(key string) string {
	node := b.Search(key)
	if node != nil {
		k := 0
		for index := 0; index < len(node.Keys); index++ {
			if node.Keys[index].Key == key {
				k = index
				break
			}
		}
		child := node.Children[k+1]

		predecessor := child.Keys[0].Key
		return predecessor
	}
	return ""
}

// Funkcija koja deli dete na dva nova cvora
func (b *BTree) SplitChild(node *BTreeNode) {
	newNode := NewBTreeNode(make([]*BTreeKey, 0))
	node.Parent.AddKey(node.FindMedian())
	list := make([]*BTreeKey, len(node.Keys))
	copy(list, node.Keys)
	node.Keys = list[:b.M-1]
	newNode.Keys = list[b.M:]
	node.Parent.AddChild(newNode)
	if !(node.IsLeaf) {
		list := make([]*BTreeNode, len(node.Children))
		copy(list, node.Children)
		node.Children = list[:b.M]
		newNode.Children = list[b.M:]
		newNode.IsLeaf = false
		//Postavljamo odgovarajuceg Parenta novom nodu
		for _, childNode := range newNode.Children {
			childNode.Parent = newNode
		}
	}
}

func (b *BTree) InsertSplit(key string, value string, node *BTreeNode) {
	//Ako roditelj nema max broj kljuceva, kljuc se dodaje roditelju
	if !(node.Parent.CheckMaxKeys(b.M)) {
		node.AddKey(key, value)
		median, mValue := node.FindMedian()
		node.RemoveKey(median)
		node.Parent.AddKey(median, mValue)
		n := len(node.Keys)
		mid := (n - 1) / 2
		list := make([]*BTreeKey, len(node.Keys))
		copy(list, node.Keys)
		node.Keys = list[:mid]
		newChildKeys := list[mid:]
		newChildNode := NewBTreeNode(newChildKeys)
		node.Parent.AddChild(newChildNode)
		b.CurrentSize += 1
		//U suprotnom rekurzivno pozivamo za roditelja
	} else {
		// medianParent, mParentValue := node.Parent.FindMedian()
		b.SplitChild(node.Parent)
		// b.InsertSplit(medianParent, mParentValue, node.Parent)
		node.AddKey(key, value)
		median, mValue := node.FindMedian()
		node.RemoveKey(median)
		node.Parent.AddKey(median, mValue)
		n := len(node.Keys)
		mid := (n - 1) / 2
		list := make([]*BTreeKey, len(node.Keys))
		copy(list, node.Keys)
		node.Keys = list[:mid]
		newChildKeys := list[mid:]
		newChildNode := NewBTreeNode(newChildKeys)
		node.Parent.AddChild(newChildNode)
		b.CurrentSize += 1
	}
}

func (b *BTree) Insert(key string, value string) {
	if b.CurrentSize == b.MaxSize {
		fmt.Println("B-Stablo je maksimalno popunjeno")
		return
	}
	//Ukoliko kljuc postoji ne moramo da vrsimo dodavanje
	if b.Search(key) != nil {
		return
	}
	node := b.Root.SearchInsert(key)

	//Ukoliko odgovarajuci cvor nema vise od MAX el. samo izvrsimo dodavanje
	if !(node.CheckMaxKeys(b.M)) {
		node.AddKey(key, value)
		//Ako je i koren pun, dodajemo novi koren
	} else if b.Root.CheckMaxKeys(b.M) {
		oldRoot := b.Root
		newRoot := NewBTreeNode(make([]*BTreeKey, 0))
		b.Root = newRoot
		newRoot.AddChild(oldRoot)
		b.SplitChild(oldRoot)
		node := b.Root.SearchInsert(key)
		if !(node.CheckMaxKeys(b.M)) {
			node.AddKey(key, value)
			b.CurrentSize += 1
		} else {
			b.InsertSplit(key, value, node)
		}

		//U suportonom rekurzivno pozovemo funkciju
	} else {
		node := b.Root.SearchInsert(key)
		if !(node.CheckMaxKeys(b.M)) {
			node.AddKey(key, value)
			b.CurrentSize += 1
		} else {
			b.InsertSplit(key, value, node)
		}
	}
}

func (b *BTree) Delete(key string) bool {
	node := b.Search(key)
	if node == nil {
		return false
	}
	for _, bTreeKey := range node.Keys {
		if bTreeKey.Key == key {
			bTreeKey.Tumbstone = true
			bTreeKey.Timestamp = time.Now()
			return true
		}
	}
	return false
}

func (b *BTree) Edit(key string, value string) bool {
	node := b.Search(key)
	if node == nil {
		return false
	}
	for _, bTreeKey := range node.Keys {
		if bTreeKey.Key == key {
			bTreeKey.Value = value
			bTreeKey.Timestamp = time.Now()
			return true
		}
	}
	return false
}

/*
func (b *BTree) Delete(key int) {
	//Ukoliko kljuc ne postoji ne moramo da vrsimo brisanje
	if b.Search(key) == nil {
		reTime
	}

	//Gledamo da li je leaf
	node := b.Search(key)
	if node.IsLeaf {
		//Proverimo da li je minimalan broj kljuceva u nodu, ako ne samo brisemo
		if !(node.CheckMinKeys(b.M)) {
			node.RemoveKey(key)
		} else { //u suprotnom preuzimamo iz cvora pored
			index := 0
			parent := node.Parent
			for i, value := range parent.Children {
				if value == node {
					index = i
					break
				}
			}
			if index != 0 {
				//Preuzimanje iz levog sibling cvora
				leftSibling := parent.Children[index-1]
				if !(leftSibling.CheckMinKeys(b.M)) {
					parentKey := parent.Keys[index-1]
					node.RemoveKey(key)
					node.AddKey(parentKey)
					leftSiblingKey := b.Predecessor(parentKey)
					parent.Keys[index-1] = leftSiblingKey
					leftSibling.RemoveKey(leftSiblingKey)
					return
				}
			}
			//TODO: Treba rekurzivno pozvati ukoliko ima vise od troje dece...
			if index != len(node.Keys)-1 {
				//Preuzimanje iz desnog sibling cvora
				rightSibling := parent.Children[index+1]
				if !(rightSibling.CheckMinKeys(b.M)) {
					parentKey := parent.Keys[index]
					node.RemoveKey(key)
					node.AddKey(parentKey)
					rightSiblingKey := b.Successor(parentKey)
					parent.Keys[index] = rightSiblingKey
					rightSibling.RemoveKey(rightSiblingKey)
					return
				}
			}

		}
	}

}
*/

func (tree *BTree) PrintTree(node *BTreeNode) {
	if node.IsLeaf {
		for _, Bkey := range node.Keys {
			fmt.Println(Bkey.Key + ", " + Bkey.Value)
		}
	} else {
		//Za decu do posledneg deteta
		for i := 0; i < len(node.Keys); i++ {
			tree.PrintTree(node.Children[i])
			fmt.Println(node.Keys[i].Key + ", " + node.Keys[i].Value)
		}
		//Za poslednje dete jer dece ima za 1 vise
		tree.PrintTree(node.Children[len(node.Keys)])
	}

}
func (tree *BTree) ReturnAllValid() []EL.Element {
	var listElement []EL.Element
	listElement = tree.AppendKeys(tree.Root, listElement)
	return listElement
}

func (tree *BTree) AppendKeys(node *BTreeNode, listElement []EL.Element) []EL.Element {
	if node.IsLeaf {
		for _, Bkey := range node.Keys {
			element := EL.NewElement(Bkey.Tumbstone, Bkey.Timestamp, Bkey.Key, Bkey.Value)
			listElement = append(listElement, *element)
		}
	} else {
		for i := 0; i < len(node.Keys); i++ {
			listElement = tree.AppendKeys(node.Children[i], listElement)
			element := EL.NewElement(node.Keys[i].Tumbstone, node.Keys[i].Timestamp, node.Keys[i].Key, node.Keys[i].Value)
			listElement = append(listElement, *element)
		}
		//Za poslednje dete jer dece ima za 1 vise
		listElement = tree.AppendKeys(node.Children[len(node.Keys)], listElement)
	}
	return listElement
}
