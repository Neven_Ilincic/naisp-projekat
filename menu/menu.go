package menu

import (
	LRU "LRUCache"
	WAL "WAL"
	"bufio"
	EL "element"
	"fmt"
	LSM "lsm"
	MT "memTable"
	"os"
	SST "sstable"
	"strconv"
	"strings"
	"time"
	TB "tokenBucket"
	"utils"
)

// Pocetni meni
func StartingMenu(config utils.Config, mth *MT.MemTablesHandler, lsm *LSM.LSM, cache *LRU.LRUCache, tb *TB.TokenBucket, p, seg int) bool {
	fmt.Println("Dobrodosli, izaberite opciju!")
	fmt.Println("1. PUT operacija")
	fmt.Println("2. DELETE operacija")
	fmt.Println("3. GET operacija")
	fmt.Println("0. IZLAZ")

	reader := bufio.NewReader(os.Stdin)

	option, _ := reader.ReadString('\n')

	option = strings.TrimSpace(option)
	switch option {
	case "1":
		possible := tb.Consume(1)
		fmt.Println("Preostali tokeni: ", tb.Tokens)
		if possible {
			PutMenu(config, mth, lsm, cache, tb, p, seg)
		} else {
			fmt.Println("Nema dovoljno tokena.")
			break
		}
	case "2":
		possible := tb.Consume(1)
		fmt.Println("Preostali tokeni: ", tb.Tokens)
		if possible {
			DeleteMenu(config, mth, lsm, cache, tb, p, seg)
		} else {
			fmt.Println("Nema dovoljno tokena.")
			break
		}
	case "3":
		possible := tb.Consume(1)
		fmt.Println("Preostali tokeni: ", tb.Tokens)
		if possible {
			GetMenu(config, mth, lsm, cache, tb)
		} else {
			fmt.Println("Nema dovoljno tokena.")
			break
		}
	case "0":
		tb.SerializeTokenBucket("TokenBucket/tokenBucket/tb.db")
		index := (*mth).FindFirstEmpty()
		if index == 0 {
			WAL.WriteMetaData("WAL/metadata.txt", "memtable_"+strconv.Itoa(index+1), seg, p)
		}
		return false
	default:
		fmt.Println("Nepoznata opcija. Molimo izaberite ponovo.")
		return true
	}
	return true
}

func PutMenu(config utils.Config, mth *MT.MemTablesHandler, lsm *LSM.LSM, cache *LRU.LRUCache, tb *TB.TokenBucket, p, seg int) {
	fmt.Println("----PUT----")
	//Kljuc
	fmt.Println("Unesite kljuc: ")
	reader := bufio.NewReader(os.Stdin)
	key, _ := reader.ReadString('\n')
	key = strings.TrimSpace(key)

	//Vrednost
	fmt.Println("Unesite vrednost: ")
	reader = bufio.NewReader(os.Stdin)
	value, _ := reader.ReadString('\n')
	value = strings.TrimSpace(value)
	Operation(key, value, false, config, *mth, lsm, cache, tb, p, seg)

}

func DeleteMenu(config utils.Config, mth *MT.MemTablesHandler, lsm *LSM.LSM, cache *LRU.LRUCache, tb *TB.TokenBucket, p, seg int) {
	fmt.Println("----DELETE----")
	//Kljuc
	fmt.Println("Unesite kljuc: ")
	reader := bufio.NewReader(os.Stdin)
	key, _ := reader.ReadString('\n')
	key = strings.TrimSpace(key)

	//Vrednost
	value := ""

	Operation(key, value, true, config, *mth, lsm, cache, tb, p, seg)
}

func GetMenu(config utils.Config, mth *MT.MemTablesHandler, lsm *LSM.LSM, cache *LRU.LRUCache, tb *TB.TokenBucket) {
	fmt.Println("----GET----")
	//Kljuc
	fmt.Println("Unesite kljuc: ")
	reader := bufio.NewReader(os.Stdin)
	key, _ := reader.ReadString('\n')

	key = strings.TrimSpace(key)
	value, found := GetOperation(key, config, mth, lsm, cache, tb)
	if found {
		fmt.Println("Vrednost kljuca: ", value)
		return
	} else {
		fmt.Println("Ne postoji trazeni kljuc!")
		return
	}
}

func GetOperation(key string, config utils.Config, mth *MT.MemTablesHandler, lsm *LSM.LSM, cache *LRU.LRUCache, tb *TB.TokenBucket) (string, bool) {
	//MemTable
	memTableIndex, found := (*mth).FindIndexContains(key) // indeks memtabele

	if found {
		if config.MemTable.Structure == "SkipList" {
			memTables := (*mth).GetMemTablesSkipList()
			memTable := *memTables[memTableIndex]
			skipList := memTable.GetSkipList()
			node := skipList.Search(key)
			return node.Element.Value, true
		} else if config.MemTable.Structure == "HashMap" {
			memTables := (*mth).GetMemTablesHashMap()
			memTable := *memTables[memTableIndex]
			hashMap := memTable.GetHashMap()
			node := (*hashMap)[key]
			return node.Value, true
		} else if config.MemTable.Structure == "B-Tree" {
			memTables := (*mth).GetMemTablesBTree()
			memTable := *memTables[memTableIndex]
			bTree := memTable.GetBTree()
			node := bTree.Search(key)
			for bkey := range node.Keys {
				if node.Keys[bkey].Key == key {
					return node.Keys[bkey].Value, true
				}
			}

		}

	}

	//Cache
	foundElement, foundCache := cache.Get(key)
	if foundCache {
		return foundElement.Value, true
	}

	//SSTable
	for level := range lsm.Stats {
		for gen := range *lsm.Stats[level] {
			if (*lsm.Stats[level])[gen] != 0 {
				sstable := SST.NewSSTableLoad((*lsm.Stats[level])[gen], level+1)
				keyBytes := []byte(key)
				check := sstable.Filter.Contains(keyBytes)
				fmt.Println(check)
				if check {

					sstable.Summary.LoadSummary()
					offset, lastKey := sstable.Summary.Find(key)
					dataOffset := sstable.Index.FindKey(key, offset, lastKey)
					if dataOffset == -1 {
						continue
					}
					dataEntry, correct := sstable.Data.LoadData(dataOffset)
					if !correct {
						continue
					}
					indexFilePath := sstable.Index.FilePath
					indexFile, _ := os.Open(indexFilePath)
					keys := sstable.Index.ReturnAllRecords(indexFile)
					correctMT := sstable.MetaData.CheckMerkleTree(keys)
					fmt.Println(correctMT)
					if !correctMT {
						fmt.Println("Tabela u kojoj se nalazi element ima ostecene podatke")
						return "", false
					}

					if dataEntry.Tombstone == 0 {
						elementTombstone := false
						element := EL.NewElement(elementTombstone, dataEntry.Timestamp, key, dataEntry.Value)
						cache.Put(*element)
						return dataEntry.Value, true
					} else {
						continue
					}
				} else {
					continue

				}
			}
		}
	}
	return "", false
}

func Operation(key string, value string, deleted bool, config utils.Config, mth MT.MemTablesHandler, lsm *LSM.LSM, cache *LRU.LRUCache, tb *TB.TokenBucket, p, seg int) {
	totalSegments, _ := WAL.CountSegments("WAL/Segments")
	_, written := WAL.WriteRecord(key, value, deleted, totalSegments, config.WAL.MaxSegmentSize)

	if !written {
		fmt.Println("Neuspesno upisivanje u WAL !")
		StartingMenu(config, &mth, lsm, cache, tb, p, seg)
		return
	}

	tombstone := time.Now()
	mth.WriteToMemtable(key, value, deleted, tombstone, config, lsm, p)
	/// Zapisuje se sada u memtabelu
}
