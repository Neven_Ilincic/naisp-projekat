package bloomfilter

import (
	"crypto/md5"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"math"
	"os"
	"time"
)

type BloomFilter struct {
	Size    int    //velicina niza
	NumHash int    //broj hesh funkcija
	Set     []bool //niz gde ce se umesto 0 i 1 koristiti true i false
	Hashes  []Hash //niz hash funkcija
}

func NewBloomFilter(expectedElements int, falsePositiveRate float64) *BloomFilter {
	if expectedElements <= 0 || falsePositiveRate <= 0 {
		fmt.Println("Predvidjen broj elemenata i zeljeni fals positiv ne smeju biti manji od nule ")
		return nil
	}
	//kreiranje novog bloom filtera
	size, numHash := CalcSizeNumHash(expectedElements, falsePositiveRate)
	set := make([]bool, size)
	for i := range set {
		set[i] = false
	}
	hashes := CreateHashFunctions(uint(numHash))

	return &BloomFilter{
		Size:    size,
		NumHash: numHash,
		Set:     set,
		Hashes:  hashes,
	}

}

func CalcSizeNumHash(expectedElements int, falsePositiveRate float64) (int, int) {
	// na osnovu predvidjenog broja elemenata i zeljenog fals positiva odredjuje optimalnu velicinu seta i broj hash funkcija
	const ln2 = 0.69314718056
	size := int(-float64(expectedElements) * math.Log(falsePositiveRate) / (ln2 * ln2))
	numHash := int(float64(size) / float64(expectedElements) * ln2)

	return size, numHash
}

func (bf *BloomFilter) Add(data []byte) {
	//pronalazi hash vrednost od elementa za svaku hash funkciju zatim nalazi indeks i menja set
	for _, hash := range bf.Hashes {
		h := hash.Hash(data)
		i := h % uint64(bf.Size)
		if bf.Set[i] == true {
			continue
		} else {
			bf.Set[i] = true
		}

	}
}

func (bf *BloomFilter) Contains(data []byte) bool {
	// vraca false ako se element sigurno ne nalazi u setu ili true ako se mozda nalazi
	for _, hash := range bf.Hashes {
		h := hash.Hash(data)
		i := h % uint64(bf.Size)
		if !bf.Set[i] {
			return false
		}

	}
	return true
}

type Hash struct {
	Seed []byte
}

func (hash Hash) Hash(data []byte) uint64 {
	// funkcija vraca hash za dati podatak koristeci md5 biblioteku
	hashF := md5.New()
	hashF.Write(append(data, hash.Seed...))
	return binary.BigEndian.Uint64(hashF.Sum(nil))
}

func CreateHashFunctions(numHashes uint) []Hash {
	// funkcija vraca listu hash funkcija velicine numHashes
	hashes := make([]Hash, numHashes)      //niz nash funkcija koje vraca funkcija
	ts := uint(time.Now().Unix())          //trenutno vreme pretvoreno u int
	for i := uint(0); i < numHashes; i++ { //prvai se numHashes broj hash funkcija tako da svaka ima razlicit seed
		seed := make([]byte, 32)                       //to se postize sto se uzme trenutno vreme ts tipa uinti na njega se doda i
		binary.BigEndian.PutUint32(seed, uint32(ts+i)) //na primer ako je 1699088618 trenutno vreme raspod seed-ova ce biti od 1699088618 do 4294967295 zbog uint 32
		newHash := Hash{                               //pravi objekat Hash za svaku hash funkciju zatim je dodaje u listu i tu listu vraca
			Seed: seed,
		}
		hashes[i] = newHash
	}
	return hashes

}

func (bf *BloomFilter) ResetToNeutral() {
	for i := range bf.Set {
		bf.Set[i] = false
	}
}

func (bf BloomFilter) Serijalizacija(putanja string) {

	fajl, err := os.OpenFile(putanja, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("Greška prilikom otvaranja fajla!")
		return
	}
	defer fajl.Close()

	data, err := json.Marshal(bf)
	if err != nil {
		fmt.Println("Greška prilikom serijalizacije:", err)
		return
	}

	_, err = fajl.Write(data)
	if err != nil {
		fmt.Println("Greška prilikom pisanja u fajl:", err)
	}

	fmt.Println("BloomFilter objekat uspešno serijalizovan u fajl sa ekstenzijom")

}

func Deserijalizacija(putanja string) (*BloomFilter, error) {

	data, err := os.ReadFile(putanja)
	if err != nil {
		return nil, err
	}

	var bf BloomFilter
	err = json.Unmarshal(data, &bf)
	if err != nil {
		return nil, err
	}

	return &bf, nil
}
