package sstable

import (
	WAL "WAL"
	"encoding/json"
	"fmt"
	"os"
	"time"
)

type DataEntry struct {
	CRC       int32
	Timestamp time.Time
	Tombstone byte
	Value     string
}

func NewDataEntry(CRC int32, Timestamp time.Time, Tombstone byte, Value string) *DataEntry {
	return &DataEntry{
		CRC:       CRC,
		Timestamp: Timestamp,
		Tombstone: Tombstone,
		Value:     Value,
	}
}

type Data struct {
	FilePath string
	Entries  []*DataEntry
}

func NewData(FilePath string) *Data {
	return &Data{
		FilePath: FilePath,
		Entries:  make([]*DataEntry, 0),
	}
}

func (data *Data) LoadData(offset int64) (*DataEntry, bool) {
	empty := NewDataEntry(0, time.Now(), 0, "")

	file, err := os.Open(data.FilePath)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return empty, false
	}
	defer file.Close()

	_, err = file.Seek(offset, 0)
	if err != nil {
		fmt.Println("Error seeking to offset:", err)
		return empty, false
	}

	//CRC
	var CRC int32
	buffer := make([]byte, 4)
	numRead, err := file.Read(buffer)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return empty, false
	}

	if numRead != 4 {
		return empty, false
	}

	for i := 0; i < len(buffer); i++ {
		CRC |= int32(buffer[i]) << (8 * uint(i))
	}

	//Timestamp
	buffer2 := make([]byte, 27)
	numRead2, err := file.Read(buffer2)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return empty, false
	}

	if numRead2 != 27 {
		return empty, false
	}

	var timestamp time.Time
	if err := json.Unmarshal(buffer2, &timestamp); err != nil {
		fmt.Println("Error decoding time from bytes:", err)
		return empty, false
	}

	//Tombstone
	var tombstone byte
	buffer3 := make([]byte, 1)
	numRead3, err := file.Read(buffer3)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return empty, false
	}

	if numRead3 != 1 {
		return empty, false
	}

	for i := 0; i < len(buffer3); i++ {
		tombstone |= byte(buffer3[i]) << (8 * uint(i))
	}

	//ValueSize
	var valueSize int64
	buffer4 := make([]byte, 8)
	numRead4, err := file.Read(buffer4)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return empty, false
	}

	if numRead4 != 8 {
		return empty, false
	}

	for i := 0; i < len(buffer4); i++ {
		valueSize |= int64(buffer4[i]) << (8 * uint(i))
	}

	//Value
	var valueString string
	buffer5 := make([]byte, valueSize)
	numRead5, err := file.Read(buffer5)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return empty, false
	}

	if numRead5 != int(valueSize) {
		return empty, false
	}

	for _, b := range buffer5 {
		valueString += string(b)
	}

	crcBytes := WAL.HashCRC(buffer5)
	var CRCValue int32
	for i := 0; i < len(crcBytes); i++ {
		CRCValue |= int32(crcBytes[i]) << (8 * uint(i))
	}

	if CRC == CRCValue {
		//DataEntry
		dataEntry := NewDataEntry(CRC, timestamp, tombstone, valueString)
		return dataEntry, true
	} else {
		return empty, false
	}
}

func LoadOneELement(file *os.File) (string, time.Time, bool, error) {

	buffer2 := make([]byte, 27)
	numRead2, err := file.Read(buffer2)
	if err != nil {
		if err.Error() == "EOF" {
			return "", time.Now(), false, err
		}
		fmt.Println("Error reading file:", err)
		return "", time.Now(), false, err
	}

	if numRead2 != 27 {
		return "", time.Now(), false, err
	}

	var timestamp time.Time
	if err := json.Unmarshal(buffer2, &timestamp); err != nil {
		fmt.Println("Error decoding time from bytes:", err)
		return "", time.Now(), false, err
	}

	//Tombstone
	var tombstone byte
	buffer3 := make([]byte, 1)
	numRead3, err := file.Read(buffer3)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return "", time.Now(), false, err
	}

	if numRead3 != 1 {
		return "", time.Now(), false, err
	}

	for i := 0; i < len(buffer3); i++ {
		tombstone |= byte(buffer3[i]) << (8 * uint(i))
	}

	tombstoneBool := false
	if tombstone == 1 {
		tombstoneBool = true
	}

	//ValueSize
	var valueSize int64
	buffer4 := make([]byte, 8)
	numRead4, err := file.Read(buffer4)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return "", time.Now(), false, err
	}

	if numRead4 != 8 {
		return "", time.Now(), false, err
	}

	for i := 0; i < len(buffer4); i++ {
		valueSize |= int64(buffer4[i]) << (8 * uint(i))
	}

	//Value
	var valueString string
	buffer5 := make([]byte, valueSize)
	numRead5, err := file.Read(buffer5)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return "", time.Now(), false, err
	}

	if numRead5 != int(valueSize) {
		return "", time.Now(), false, err
	}

	for _, b := range buffer5 {
		valueString += string(b)
	}

	return valueString, timestamp, tombstoneBool, nil
}
