package LRUCache

import (
	"container/list"
	EL "element"
	"fmt"
)

type LRUCache struct {
	Capacity int
	Cache    map[string]*list.Element
	List     *list.List
}

// Mapa sluzi kako bi imali O(1) pristup elementu a samo dodavanje elementa na prvu poziciju je O(1)
func NewLRUCache(capacity int) *LRUCache {
	return &LRUCache{
		Capacity: capacity,
		Cache:    make(map[string]*list.Element),
		List:     list.New(),
	}
}

func (cache *LRUCache) Get(key string) (*EL.Element, bool) {
	//Vraca element sa zadatim kljucem ako on postoji i takodje ga pomera na pocetak liste kao najskorije koriscenog
	element, found := cache.Cache[key]
	if found {
		cache.List.MoveToFront(element)
		return element.Value.(*EL.Element), true
	}
	return &EL.Element{}, false
}

func (cache *LRUCache) Put(element EL.Element) {
	//Ubacuje novi element u cache
	cacheElement, found := cache.Cache[element.Key]
	//Ako element sa istim kljucem vec postoji uradice izmenu i pri tome ide na pocetak
	if found {
		cacheElement.Value.(*EL.Element).Value = element.Value
		cacheElement.Value.(*EL.Element).Timestamp = element.Timestamp
		cacheElement.Value.(*EL.Element).Tumbstone = element.Tumbstone

		cache.List.MoveToFront(cacheElement)
		return
	}

	//Slucaj ako je cache popunjen treba ukloniti najstariji koriscen element
	if cache.Capacity <= len(cache.Cache) {

		leastUsedElement := cache.List.Back()
		cache.List.Remove(leastUsedElement)
		delete(cache.Cache, leastUsedElement.Value.(*EL.Element).Key)
	}

	listElement := cache.List.PushFront(&element)
	cache.Cache[element.Key] = listElement
}

func (cache *LRUCache) EmptyCache() {
	//Prazni kes
	cache.List.Init()

	for key := range cache.Cache {
		delete(cache.Cache, key)
	}

}

func (cache *LRUCache) PrintCache() {
	//Ispis elemenata
	fmt.Println("LRUCache elementi:")
	for k, v := range cache.Cache {
		fmt.Printf("Key: %s, Value: %+v\n", k, v.Value.(*EL.Element))
	}
	fmt.Println("LRUCache redosled:")
	for element := cache.List.Front(); element != nil; element = element.Next() {
		fmt.Printf("Key: %s, Value: %+v\n", element.Value.(*EL.Element).Key, element.Value.(*EL.Element))
	}
}
