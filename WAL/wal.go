package WAL

import (
	"bufio"
	"crypto/md5"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"hash/crc32"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
	"utils"
)

/*
+---------------+-----------------+---------------+---------------+-----------------+-...-+--...--+
|    CRC (4B)   | Timestamp (8B)  | Tombstone(1B) | Key Size (8B) | Value Size (8B) | Key | Value |
+---------------+-----------------+---------------+---------------+-----------------+-...-+--...--+
CRC = 32bit hash computed over the payload using CRC
Key Size = Length of the Key data
Tombstone = If this record was deleted and has a value
Value Size = Length of the Value data
Key = Key data
Value = Value data
Timestamp = Timestamp of the operation in seconds
*/

// Kreiranje Hash funkcije
func CreateHashFunction() HashWithSeed {
	ts := uint(time.Now().Unix())
	seed := make([]byte, 32)
	binary.BigEndian.PutUint32(seed, uint32(ts))
	hash := HashWithSeed{Seed: seed}

	return hash
}

func (hash *HashWithSeed) Serialization(filePath string) error {
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("Greška prilikom otvaranja fajla!")
		return err
	}
	defer file.Close()

	data, err := json.Marshal(hash)
	if err != nil {
		fmt.Println("Greška prilikom serijalizacije:", err)
		return err
	}

	_, err = file.Write(data)
	if err != nil {
		fmt.Println("Greška prilikom pisanja u fajl:", err)
	}

	fmt.Println("Hash funkcija je uspešno serijalizovana!")
	return nil
}

func Deserialization(filePath string) (*HashWithSeed, error) {

	data, err := os.ReadFile(filePath)
	if err != nil {
		fmt.Println("Greska prilikom otvaranja fajla!")
		return nil, err
	}

	var hash HashWithSeed
	err = json.Unmarshal(data, &hash)
	if err != nil {
		fmt.Println("Greska pri deserijalizaciji hash funkcije!")
		return nil, err
	}

	return &hash, nil

}

type HashWithSeed struct {
	Seed []byte
}

func (hash HashWithSeed) Hash(data []byte) uint32 {
	// funkcija vraca hash za dati podatak koristeci md5 biblioteku
	hashF := md5.New()
	hashF.Write(append(data, hash.Seed...))
	return binary.LittleEndian.Uint32(hashF.Sum(nil))
}

// Provera da li postoji hash funkcija
func CheckHashFunction() {
	num, _ := CountSegments("WAL/Segments")
	num2, _ := CountSegments("tables")
	file, err := os.OpenFile("WAL/hesFunkcija.db", os.O_RDWR, 0666)

	if err != nil && num == 0 && num2 == 0 {
		hashFunction := CreateHashFunction()
		hashFunction.Serialization("WAL/hesFunkcija.db")
	}
	file.Close()
}

const (
	CRC_SIZE        = 4
	TIMESTAMP_SIZE  = 8
	TOMBSTONE_SIZE  = 1
	KEY_SIZE_SIZE   = 8
	VALUE_SIZE_SIZE = 8

	CRC_START        = 0
	TIMESTAMP_START  = CRC_START + CRC_SIZE
	TOMBSTONE_START  = TIMESTAMP_START + TIMESTAMP_SIZE
	KEY_SIZE_START   = TOMBSTONE_START + TOMBSTONE_SIZE
	VALUE_SIZE_START = KEY_SIZE_START + KEY_SIZE_SIZE
	KEY_START        = VALUE_SIZE_START + VALUE_SIZE_SIZE
)

func CRC32(data []byte) uint32 {
	return crc32.ChecksumIEEE(data)
}

// Funckija vraca hesirano bajtove za CRC
func HashCRC(valueBytes []byte) []byte {
	hashFunction, _ := Deserialization("WAL/hesFunkcija.db") // deserijalizujem hashFunkciju
	hashedValue := hashFunction.Hash(valueBytes)             // hesirana vrednost
	a := make([]byte, CRC_SIZE)                              // pretvaram u niz bajtova da bih mogao da zapisem u fajl
	binary.LittleEndian.PutUint32(a, hashedValue)

	return a
}

// / funckija upisuje cele segmente
func WriteSegments(startingSegment int, listOfBytes []byte, maxSegmentSize int) {
	byteSize := len(listOfBytes)
	list := listOfBytes
	currentByte := 0
	for byteSize > 0 {
		if byteSize > maxSegmentSize { // ako je vece, u segment zapisujem maxSegmentSize bajtova
			list = listOfBytes[currentByte : currentByte+maxSegmentSize]
			byteSize -= maxSegmentSize
			currentByte += maxSegmentSize
		} else {
			list = listOfBytes[currentByte:]
			byteSize = 0
		}

		//Upisivanje u fajl
		filePath := "WAL/Segments/wal_" + strconv.Itoa(startingSegment) + ".log"
		file, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			panic("Greska prilikom pisanja!")
		}
		file.Write(list)
		file.Close()
		startingSegment += 1 //znam da ce se uvek zapisivati u novi segment
	}
}

// Pisanje zapisa u fajl -- vraca da li je zapisano u novi segment i da li je uspesno upisan zapis
func WriteRecord(key string, value string, deleted bool, numSegments int, maxSegmentSize int) (bool, bool) {
	newSegment := false
	if numSegments == 0 {
		numSegments = 1
	}
	//// OVO SAM DODAO
	fileName := "WAL/Segments/wal_" + strconv.Itoa(numSegments) + ".log"
	num, _ := CountSegments("WAL/Segments")
	if num != 0 {
		fi, _ := os.Stat(fileName)
		size := fi.Size()
		if maxSegmentSize < int(size) {
			numSegments += 1
			fileName = "WAL/Segments/wal_" + strconv.Itoa(numSegments) + ".log"
		}
	}

	/////

	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
		return false, false

	}

	fileInfo, err2 := file.Stat()
	if err2 != nil {
		log.Fatal("Nemoguce ucitavanje statistike fajla!")
		return false, false
	}
	segmentSize := fileInfo.Size() //Velicina fajla(segmenta) u bajtovima

	defer file.Close()
	valueBytes := []byte(value) //vrednost koja se hesira za crc

	//CRC
	crcBytes := HashCRC(valueBytes) // hesiran CRC u bajtovima

	//TimeStamp
	currentTimeSec := uint64(time.Now().Unix()) //pretvaram u uint64
	timeStampBytes := make([]byte, TIMESTAMP_SIZE)
	binary.LittleEndian.PutUint64(timeStampBytes, currentTimeSec) //ubacujem uint64 vrednost u b niz bajtova

	//TombStone
	tombStoneBytes := make([]byte, 1) // 1 bajtna vrednost //isto sto i uint8
	if deleted {
		tombStoneBytes[0] = byte(1) // ako je deleted true, postavljam vrednost tombStona na 1
	} else {
		tombStoneBytes[0] = byte(0)
	}

	//KeySize
	keySize := uint64(len(key))                          //pronalazim velicinu key stringa
	keySizeBytes := make([]byte, KEY_SIZE_SIZE)          //pravim niz []byte sa velicinom od 8 bajta
	binary.LittleEndian.PutUint64(keySizeBytes, keySize) // vrednost keySize stavljam u []byte niz kako bih mogao da je zapisem

	//ValueSize
	valueSize := uint64(len(value))
	valueSizeBytes := make([]byte, VALUE_SIZE_SIZE)
	binary.LittleEndian.PutUint64(valueSizeBytes, valueSize)

	//Key
	keyBytes := []byte(key) // vrednost kljuca

	//Value
	//ima gore

	sizeOfRecord := 29 + int(keySize) + int(valueSize) //velicina zapisa u bajtovima
	//spajam niz bajtova u jedan zapis
	recordBytes := make([]byte, 0)
	recordBytes = append(recordBytes, crcBytes...)
	recordBytes = append(recordBytes, timeStampBytes...)
	recordBytes = append(recordBytes, tombStoneBytes...)
	recordBytes = append(recordBytes, keySizeBytes...)
	recordBytes = append(recordBytes, valueSizeBytes...)
	recordBytes = append(recordBytes, keyBytes...)
	recordBytes = append(recordBytes, valueBytes...)

	bufferedWriter := bufio.NewWriterSize(file, maxSegmentSize)

	if sizeOfRecord <= (maxSegmentSize - int(segmentSize)) {
		bufferedWriter.Write(recordBytes) // upisujem ceo zapis u trenutni segment
		bufferedWriter.Flush()
	} else {
		firstHalf := maxSegmentSize - int(segmentSize)
		firstHalfRecord := recordBytes[0:firstHalf]             //deo zapisa koji zapisujem u trenutni segment
		secondHalfRecord := recordBytes[firstHalf:sizeOfRecord] // deo zapisa koji upisujem u novi segment
		bufferedWriter.Write(firstHalfRecord)
		bufferedWriter.Flush()
		file.Close()
		newFileName := "WAL/Segments/wal_" + strconv.Itoa(numSegments+1) + ".log" // pravim novi segment, sa povecanim brojacem
		newFile, err3 := os.OpenFile(newFileName, os.O_CREATE|os.O_APPEND, 0666)

		if err3 != nil {
			log.Fatal("Greska u stvaranju novog segmenta!")
		}

		bufferedWriter := bufio.NewWriterSize(newFile, maxSegmentSize)
		bufferedWriter.Write(secondHalfRecord)
		bufferedWriter.Flush() // upisujem zapis u novi segment
		newFile.Close()
	}
	return newSegment, true
}

// Citanje celog segmenta - vraca listu sa svim bajtovima
func ReadWholeSegment(filepath string) []byte {
	file, err := os.OpenFile(filepath, os.O_RDONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close() // uradice se na kraju funkcije

	bytes, err2 := io.ReadAll(file)
	if err2 != nil {
		log.Fatal(err2)
	}

	return bytes
}

// Funkcija vraca pojedinacan zapis (vrednost) u bajtovima i pokazivac(indeks) na bajt gde se nalazim i da li je zapis u novom segmentu
func ReturnRecord(filepath string, currentPointer int, hashFunction HashWithSeed, maxSegmentSize int) ([]byte, int, bool) {

	newSegment := false
	file, err := os.OpenFile(filepath, os.O_RDONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close() // uradice se na kraju funkcije

	bytesToEnd := maxSegmentSize - currentPointer

	file.Seek(int64(currentPointer), 0) //pokazivac stavljam na pocetak novog zapisa
	movedInRecord := 0
	remainToRead := 0    //koliko bajtova zapisa mi je ostalo jos da procitam
	remainToReadNew := 0 // preostali broj bajtova zapisa u sledecem segmentu
	recordBytes := make([]byte, 0)
	//ako glavni deo zapiza nije u istom segmentu
	mainPartOfRecord := false
	partRecord := 0
	//
	if bytesToEnd > 29 { /// znam da ce se  CRC,TIMESTAMP,TOMBSTONE,KEY SIZE,VALUE SIZE celi nalaziti u istom segmentu (ukupno zauzimaju 29 bajtova)

		mainPartOfRecord = true
		file.Seek(KEY_SIZE_START, 1) // pomeranje
		movedInRecord += KEY_SIZE_START

		//Key size
		keySizeBytes := make([]byte, KEY_SIZE_SIZE)
		file.Read(keySizeBytes)                             //citanje // pomeranje
		movedInRecord += KEY_SIZE_SIZE                      // trenutno na pocetku Value Size polja // p
		keySize := binary.LittleEndian.Uint64(keySizeBytes) // kolika je velicina key value polja da bih ga preskocio
		remainToRead += int(keySize)

		//Value size
		valueSizeBytes := make([]byte, VALUE_SIZE_SIZE)
		file.Read(valueSizeBytes) // citanje // pomeranje
		movedInRecord += VALUE_SIZE_SIZE
		valueSize := binary.LittleEndian.Uint64(valueSizeBytes) // velicina value polja
		remainToRead += int(valueSize)

		file.Seek(-int64(movedInRecord), 1) // vracam pokazivac na pocetak zapisa
		firstHalfRecord := make([]byte, 29)
		file.Read(firstHalfRecord) // citanje // pomeranje
		currentPointer += 29
		recordBytes = append(recordBytes, firstHalfRecord...)

		if (remainToRead + currentPointer) <= maxSegmentSize { // ostatak zapisa je u trenutnom segmentu
			remainBytes := make([]byte, remainToRead)
			file.Read(remainBytes)
			currentPointer += remainToRead
			recordBytes = append(recordBytes, remainBytes...)
		} else { // deo je i u sledecem segmentu
			remain := maxSegmentSize - currentPointer
			remainBytes := make([]byte, remain)
			file.Read(remainBytes)
			recordBytes = append(recordBytes, remainBytes...)
			remainToReadNew += int(remainToRead - remain)
		}
	} else { // ako je < 29
		mainPartOfRecord = false
		remain := maxSegmentSize - currentPointer // ostatak do kraja segmenta
		remainBytes := make([]byte, remain)
		file.Read(remainBytes)
		recordBytes = append(recordBytes, remainBytes...)
		partRecord = 29 - remain // koliko bajtova je ostalo da se procita crc,timestamp..value size u sledecem segmentu
		remainToReadNew += partRecord

	}

	if remainToReadNew > 0 {
		newSegment = true
		currentPointer = 0 // pokazivac je sada na pocetku novog fajla
		partOne := strings.Split(filepath, "_")[1]
		segmentIndexChar := strings.Split(partOne, ".")[0]
		segmentIndex, _ := strconv.Atoi(segmentIndexChar) // parsiranje iz stringa u int

		newFilePath := "WAL/Segments/wal_" + strconv.Itoa(segmentIndex+1) + ".log"
		newFile, err2 := os.OpenFile(newFilePath, os.O_RDONLY, 066)
		if err2 != nil {
			panic(err2)
		}
		defer newFile.Close()

		newFile.Seek(0, 0)

		if mainPartOfRecord {
			remainBytes := make([]byte, remainToReadNew)
			newFile.Read(remainBytes)
			currentPointer += remainToReadNew
			recordBytes = append(recordBytes, remainBytes...) // dodavanje na ostatak zapisa iz prethodnog segmenta
		} else {
			remainBytes := make([]byte, partRecord)
			newFile.Read(remainBytes)
			currentPointer += partRecord
			recordBytes = append(recordBytes, remainBytes...)
			//Key size
			keySizeBytes := recordBytes[KEY_SIZE_START : KEY_SIZE_START+KEY_SIZE_SIZE]
			keySize := binary.LittleEndian.Uint64(keySizeBytes) // kolika je velicina key value polja da bih ga preskocio

			//Value size
			valueSizeBytes := recordBytes[VALUE_SIZE_START : VALUE_SIZE_START+VALUE_SIZE_SIZE]
			valueSize := binary.LittleEndian.Uint64(valueSizeBytes)

			toRead := keySize + valueSize
			lastRemain := make([]byte, toRead)
			newFile.Read(lastRemain)
			currentPointer += int(toRead)
			recordBytes = append(recordBytes, lastRemain...)
		}

	}
	return recordBytes, currentPointer, newSegment
}

func ReadRecord(record []byte, hashFunction HashWithSeed) (bool, time.Time, string, string) {
	tombStone := record[12]

	////CRC
	CRCbytes := record[0:CRC_SIZE]

	// Timestamp
	timestampBytes := record[TIMESTAMP_START : TIMESTAMP_START+TIMESTAMP_SIZE]
	// Key Size
	keySizeBytes := record[KEY_SIZE_START : KEY_SIZE_START+KEY_SIZE_SIZE]
	keySize := binary.LittleEndian.Uint64(keySizeBytes) // velicina key polja

	// Value Size
	valueSizeBytes := record[VALUE_SIZE_START : VALUE_SIZE_START+VALUE_SIZE_SIZE]
	valueSize := binary.LittleEndian.Uint64(valueSizeBytes) // velicina value polja

	// Key polje
	keyBytes := record[KEY_START : KEY_START+keySize]
	keyStr := string(keyBytes)
	// Value polje
	valueStart := KEY_START + keySize
	valueBytes := record[valueStart : valueStart+valueSize]
	valueStr := string(valueBytes)

	// Provera validnosti zapisa
	valueHashed := hashFunction.Hash(valueBytes)      //hesirana vrednost uint32
	value := make([]byte, CRC_SIZE)                   // hocu da smestim hesiranu vrednost u []byte velicine 4 bajta
	binary.LittleEndian.PutUint32(value, valueHashed) // prebacujem uint32 u []byte

	if CRC32(value) != CRC32(CRCbytes) {
		panic("Podatak nije validan")
	}
	timeStampNum := binary.LittleEndian.Uint64(timestampBytes)
	timeStamp := time.Unix(int64(timeStampNum), 0) // type time.time

	if tombStone == 1 { // provera da li je obrisana vrednost
		return true, timeStamp, keyStr, valueStr
	}
	return false, timeStamp, keyStr, valueStr
}

// Brisanje zapisa ---  broj obrisanih segmenata i pokazivac na pocetak nove memtabele
func RemoveRecords(numExisting int, numDeleted int, numChanged int, hashFunction HashWithSeed, maxSegmentSize int, walSize int, pointer int) (int, int) { //Broj obrisanih segmenata,pokazivac na kraj/pocetak nove memtabele

	totalToRemove := numExisting + numDeleted + numChanged // broj zapisa koji treba da se obrisu
	currentSegment := 1
	deletedSegments := 0
	for totalToRemove > 0 {
		filePath := "WAL/Segments/wal_" + strconv.Itoa(currentSegment) + ".log"

		/////////// DODATO
		file, _ := os.Stat(filePath)
		maxSegmentSize = int(file.Size())
		//////////////
		_, currentPointer, newSegment := ReturnRecord(filePath, pointer, hashFunction, maxSegmentSize)

		pointer = currentPointer
		if newSegment { // Ceo segment treba da se obrise
			DeleteSegment(filePath)
			currentSegment += 1
			deletedSegments += 1
		}
		totalToRemove -= 1
	}

	highestSegmentIndex := walSize
	newSegmentIndex := 1
	startingSegmentIndex := deletedSegments + 1
	// Preimenovanje segmenata
	for startingSegmentIndex <= highestSegmentIndex {
		filePathOld := "WAL/Segments/wal_" + strconv.Itoa(startingSegmentIndex) + ".log"
		filePathNew := "WAL/Segments/wal_" + strconv.Itoa(newSegmentIndex) + ".log"
		err := os.Rename(filePathOld, filePathNew)
		if err != nil {
			panic(err)
		}
		startingSegmentIndex += 1
		newSegmentIndex += 1
	}

	return deletedSegments, pointer
}

func DeleteSegment(filePath string) {
	err := os.Remove(filePath)
	if err != nil {
		panic(err)
	}
}

// Funkcija zapisuje velicine memtabela --- od kog segmenta pocinje i od kojeg bajta // poziva se kada se napuni memtabela
func WriteMetaData(filePath string, memtableName string, startingSegment int, pointer int) {
	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_APPEND, 066)
	if err != nil {
		panic(err)
	}
	// fileStats, _ := file.Stat()
	// fileSize := fileStats.Size()

	memtable := []byte(memtableName + " wal_" + strconv.Itoa(startingSegment) + ".log " + strconv.Itoa(pointer) + "\n")
	file.Write(memtable)
	file.Close()

}

// Citanje metadata.txt  // vraca segment od kojeg krece citanje, broj segmenata koji se citaju, pokazivac na pocetak i pokazivac na kraj memtabele (pocetak druge) // vraceni podaci se koriste za popunjavanje memtabele
func ReadMetaData(filePath string) (string, int, int, int) {
	file, err := os.OpenFile(filePath, os.O_RDONLY, 0666)
	if err != nil {
		panic("Greska u citanju metaData fajla!")
	}
	defer file.Close()

	startingPointer := 0
	endingPointer := 0
	startingSegmentPath := ""

	scanner := bufio.NewScanner(file)
	startingIndex := 0
	endingIndex := 0

	//Citam prvi red
	success := scanner.Scan()
	if success {
		line := scanner.Text()
		startingSegmentPath = strings.Split(line, " ")[1]
		startingIndexStr := strings.Split(strings.Split(startingSegmentPath, "_")[1], ".")[0]
		startingIndex, _ = strconv.Atoi(startingIndexStr)
		startingPointerStr := strings.Split(line, " ")[2]
		startingPointer, _ = strconv.Atoi(startingPointerStr)
	}

	// Citam drugi red
	success = scanner.Scan()
	if success {
		line := scanner.Text()
		endingSegmentPath := strings.Split(line, " ")[1]
		endingIndexStr := strings.Split(strings.Split(endingSegmentPath, "_")[1], ".")[0]
		endingIndex, _ = strconv.Atoi(endingIndexStr)
		endingPointerStr := strings.Split(line, " ")[2]
		endingPointer, _ = strconv.Atoi(endingPointerStr)
	}

	numSegments := endingIndex - startingIndex + 1 // Broj segmenta koji se citaju
	if endingPointer == 0 {
		numSegments -= 1
	}
	return startingSegmentPath, numSegments, startingPointer, endingPointer
}

// Funkcija brise prvi zapis u metaData.txt --> preimenuju se indeksi od 0
func DeleteMetaData(filePath string, numDeletedSegments int) { // Koliko segmenta obrisem, toliko se pocetak svake memtabele smanjuje za toliko segmenta npr. wal_5.log 10 --- > 3 segmenta ---> wal_2.log 10
	file, err := os.OpenFile(filePath, os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	_ = scanner.Scan() // Preskacem prvi red

	lines := []string{} //Prazna lista
	// Smestam sve ostale redove u listu kako bi ih zapisao
	for true {
		success := scanner.Scan()
		if success == false {
			break
		}
		line := scanner.Text()
		lines = append(lines, line)
	}

	newLines := []string{} // Smestam "nove" zapise
	for i := 0; i < len(lines); i++ {
		line := lines[i]
		//Menajm naziv memtabele
		memtableName := strings.Split(line, " ")[0]
		memtable := strings.Split(memtableName, "_")[0]
		memtableIndex, _ := strconv.Atoi(strings.Split(memtableName, "_")[1])
		memtableIndex -= 1
		memtableIndexStr := strconv.Itoa(memtableIndex)
		newMemtableName := memtable + "_" + memtableIndexStr

		//Menjam naziv segmenta
		segment := strings.Split(line, " ")[1]
		walPart := strings.Split(segment, ".")[0]
		logPart := strings.Split(segment, ".")[1]
		wal := strings.Split(walPart, "_")[0]
		segmentIndex, _ := strconv.Atoi(strings.Split(walPart, "_")[1])
		segmentIndex -= numDeletedSegments
		segmentIndexStr := strconv.Itoa(segmentIndex)
		newSegmentName := wal + "_" + segmentIndexStr + "." + logPart

		pointerStr := strings.Split(line, " ")[2]

		//Nova linija
		newLine := newMemtableName + " " + newSegmentName + " " + pointerStr + "\n"
		newLines = append(newLines, newLine)

	}

	file.Truncate(0)
	file.Seek(0, 0)
	//Pisanje u fajl
	for i := 0; i < len(newLines); i++ {
		file.Write([]byte(newLines[i]))
	}

}

func CountSegments(directoryPath string) (int, error) {
	files, err := os.ReadDir(directoryPath)
	if err != nil {
		return 0, err
	}

	count := 0
	for range files {
		count++
	}

	return count, nil
}

func TruncateFile(filepath string) {
	file, err := os.OpenFile(filepath, os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Greska pri brisanju sadrzaja iz metadata.txt", err)
		return
	}
	defer file.Close()

	newSize := int64(0)
	err = file.Truncate(newSize)
	if err != nil {
		fmt.Println("Greska pri brisanju sadrzaja iz metadata.txt", err)
		return
	}
}

func ReturnLastMetaData(filePath string) (int, int) {
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Greška pri otvaranju fajla (Last MetaData):", err)
		return -1, -1
	}
	defer file.Close()

	// Kreiramo scanner koji će čitati fajl
	scanner := bufio.NewScanner(file)
	var lastLine string
	startingSegmentPath := ""
	startingIndex := 0
	startingPointer := 0

	// Čitamo fajl red po red
	for scanner.Scan() {
		lastLine = scanner.Text()
	}
	if lastLine == "" {
		return -1, -1
	}

	startingSegmentPath = strings.Split(lastLine, " ")[1]
	startingIndexStr := strings.Split(strings.Split(startingSegmentPath, "_")[1], ".")[0]
	startingIndex, _ = strconv.Atoi(startingIndexStr)
	startingPointerStr := strings.Split(lastLine, " ")[2]
	startingPointer, _ = strconv.Atoi(startingPointerStr)

	return startingIndex, startingPointer
}

func FindPointer(num int, config utils.Config) (int, int) {
	lastSeg, lastPoi := ReturnLastMetaData("WAL/metadata.txt")
	if lastSeg == -1 && lastPoi == -1 {
		return 1, 0
	}
	pointer := lastPoi
	hash, _ := Deserialization("WAL/hesFunkcija.db")
	numSegments, _ := CountSegments("WAL/Segments")
	maxSegmentSize := 0
	newSegment := false

	for i := 1; i <= num; i += 1 {
		filePath := "WAL/Segments/wal_" + strconv.Itoa(lastSeg) + ".log"

		if lastSeg == numSegments {
			maxSegmentSize = config.WAL.MaxSegmentSize
		} else {
			file, _ := os.Stat(filePath)
			maxSegmentSize = int(file.Size())
		}

		_, pointer, newSegment = ReturnRecord(filePath, pointer, *hash, maxSegmentSize)

		if newSegment {
			lastSeg += 1
		}
	}

	return lastSeg, pointer

}
