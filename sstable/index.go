package sstable

import (
	"fmt"
	"os"
)

type IndexEntry struct {
	Key    string
	Offset int
}

func NewIndexEntry(key string, offset int) *IndexEntry {
	return &IndexEntry{
		Key:    key,
		Offset: offset,
	}
}

type Index struct {
	FilePath string
	Entries  []*IndexEntry
}

func NewIndex(FilePath string) *Index {
	return &Index{
		FilePath: FilePath,
		Entries:  make([]*IndexEntry, 0),
	}
}

//Uraditi upisivanje u fajl

func (index *Index) FindKey(key string, offset int64, end string) (dataOffset int64) {

	file, err := os.Open(index.FilePath)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	_, err = file.Seek(offset, 0)
	if err != nil {
		fmt.Println("Error seeking to offset:", err)
		return
	}

	//KeySize
	keySizeBytes := 8
	for {
		buffer := make([]byte, keySizeBytes)
		_, err := file.Read(buffer)
		if err != nil {
			fmt.Println("Error reading file:", err)
			return
		}

		var keySize int64
		for i := 0; i < len(buffer); i++ {
			keySize |= int64(buffer[i]) << (8 * uint(i))
		}

		buffer2 := make([]byte, keySize)
		numRead2, err := file.Read(buffer2)
		if err != nil {
			fmt.Println("Error reading file:", err)
			return
		}

		if numRead2 != int(keySize) {
			return
		}
		//KeyString
		var keyString string
		for _, b := range buffer2 {
			keyString += string(b)
		}

		buffer3 := make([]byte, 8)
		numRead3, err := file.Read(buffer3)
		if err != nil {
			fmt.Println("Error reading file:", err)
			return
		}

		if numRead3 != 8 {
			return
		}
		//Dataoffset
		var dataOff int64
		for i := 0; i < len(buffer3); i++ {
			dataOff |= int64(buffer3[i]) << (8 * uint(i))
		}

		if key == keyString {
			return dataOff
		}

		if keyString >= end {
			return -1
		}
	}
}

func (index *Index) ReturnAllRecords(file *os.File) []string {
	keys := make([]string, 0)
	var err error
	var key string

	for {
		key, err = ReturnOneRecord(file)
		if err != nil {
			break
		}
		keys = append(keys, key)
		file.Seek(8, 1)
	}
	return keys
}

func ReturnOneRecord(file *os.File) (string, error) {
	keySizeBytes := 8
	buffer := make([]byte, keySizeBytes)
	_, err := file.Read(buffer)
	if err != nil {
		if err.Error() == "EOF" {
			return "", err
		}
		fmt.Println("Error reading file:", err)
		return "", err
	}

	var keySize int64
	for i := 0; i < len(buffer); i++ {
		keySize |= int64(buffer[i]) << (8 * uint(i))
	}

	buffer2 := make([]byte, keySize)
	numRead2, err := file.Read(buffer2)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return "", err
	}

	if numRead2 != int(keySize) {
		return "", err
	}
	//KeyString
	var keyString string
	for _, b := range buffer2 {
		keyString += string(b)
	}

	return keyString, nil
}
