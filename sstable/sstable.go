package sstable

import (
	MT "MerkleStablo"
	WAL "WAL"
	BF "bloomfilter"
	EL "element"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
)

type SSTable struct {
	Filter   BF.BloomFilter
	Summary  Summary
	Index    Index
	Data     Data
	MetaData MT.MerkleTree
	Level    int
}

func NewSSTable(generation int, level int, expectedElements int, falsePositiveRate float64, elements []EL.Element) *SSTable {
	generationString := strconv.Itoa(generation)

	bloomfilter := BF.NewBloomFilter(expectedElements, falsePositiveRate)
	summary := NewSummary("", "", "tables/usertable-"+generationString+"-summarry.db")
	index := NewIndex("tables/usertable-" + generationString + "-index.db")
	data := NewData("tables/usertable-" + generationString + "-data.db")

	keys := make([]string, 0)
	for i := 0; i < len(elements); i += 1 {
		keys = append(keys, elements[i].Key)
	}

	metaData := MT.NewMerkleTree(keys)

	return &SSTable{
		Filter:   *bloomfilter,
		Summary:  *summary,
		Index:    *index,
		Data:     *data,
		MetaData: *metaData,
		Level:    level,
	}
}

// DODATO
func NewSSTableLoad(generation int, level int) *SSTable {
	generationString := strconv.Itoa(generation)

	bloomfilter, _ := BF.Deserijalizacija("tables/usertable-" + generationString + "-filter.db")
	summary := NewSummary("", "", "tables/usertable-"+generationString+"-summarry.db")
	index := NewIndex("tables/usertable-" + generationString + "-index.db")
	data := NewData("tables/usertable-" + generationString + "-data.db")

	metaData, _ := MT.DeserializeMerkleTree("tables/usertable-" + generationString + "-metadata.db")

	return &SSTable{
		Filter:   *bloomfilter,
		Summary:  *summary,
		Index:    *index,
		Data:     *data,
		MetaData: *metaData,
		Level:    level,
	}
}

func (sstable *SSTable) WriteTable(elements []EL.Element, thinning int, generation int) {
	generationString := strconv.Itoa(generation)
	//dataSet := make([]string, 0)
	indexFile, err := os.OpenFile(sstable.Index.FilePath, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("Error opening indexFile:", err)
		return
	}
	defer indexFile.Close()

	dataFile, err := os.OpenFile(sstable.Data.FilePath, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("Error opening dataFile:", err)
		return
	}
	defer dataFile.Close()

	sstable.Summary.FirstKey = elements[0].Key
	sstable.Summary.LastKey = elements[len(elements)-1].Key

	counter := 1
	for _, element := range elements {

		// Sumarry
		if counter%thinning == 0 {
			indexOffset, err := indexFile.Seek(0, io.SeekCurrent)
			if err != nil {
				fmt.Println("Error getting offset:", err)
				return
			}
			sstable.Summary.AddEntry(element.Key, indexOffset)
		}

		// Index
		keyBytes := []byte(element.Key)
		keySize := len(keyBytes)
		keySize64 := int64(keySize)
		bufferKeySize := make([]byte, 8)
		binary.LittleEndian.PutUint64(bufferKeySize, uint64(keySize64))
		_, err = indexFile.Write(bufferKeySize)
		if err != nil {
			panic(err)
		}

		indexFile.Write(keyBytes)

		dataOffset, err := dataFile.Seek(0, io.SeekCurrent)
		if err != nil {
			fmt.Println("Error getting offset:", err)
			return
		}
		dataOffset64 := int64(dataOffset)
		bufferDataOffset := make([]byte, 8)
		binary.LittleEndian.PutUint64(bufferDataOffset, uint64(dataOffset64))
		_, err = indexFile.Write(bufferDataOffset)
		if err != nil {
			panic(err)
		}

		//Data
		valueBytes := []byte(element.Value) //vrednost koja se hesira za crc
		crcBytes := WAL.HashCRC(valueBytes) // hesiran CRC u bajtovima
		dataFile.Write(crcBytes)
		////TimeStamp
		timestampBytes, err := json.Marshal(element.Timestamp)
		if err != nil {
			fmt.Println("Error encoding time to bytes:", err)
			return
		}
		dataFile.Write(timestampBytes)
		///timeStampBytes := make([]byte, TIMESTAMP_SIZE)
		//binary.LittleEndian.PutUint64(timeStampBytes, currentTimeSec) //ubacujem uint64 vrednost u b niz bajtova

		////TombStone
		tombStoneBytes := make([]byte, 1) // 1 bajtna vrednost //isto sto i uint8
		if element.Tumbstone {
			tombStoneBytes[0] = byte(1) // ako je deleted true, postavljam vrednost tombStona na 1
		} else {
			tombStoneBytes[0] = byte(0)
		}
		dataFile.Write(tombStoneBytes)

		valueSize := len(valueBytes)
		valueSize64 := int64(valueSize)
		bufferValueSize := make([]byte, 8)
		binary.LittleEndian.PutUint64(bufferValueSize, uint64(valueSize64))
		_, err = dataFile.Write(bufferValueSize)
		if err != nil {
			panic(err)
		}

		dataFile.Write(valueBytes)

		sstable.Filter.Add(keyBytes)

		// dataSet = append(dataSet, element.Key)

		counter += 1
	}

	sstable.Summary.WriteSummary()
	sstable.Filter.Serijalizacija("tables/usertable-" + generationString + "-filter.db")
	// merkleTree := MT.NewMerkleTree(dataSet)
	// sstable.MetaData = *merkleTree
	sstable.MetaData.SerializeMerkleTree("tables/usertable-" + generationString + "-metadata.db")

}
