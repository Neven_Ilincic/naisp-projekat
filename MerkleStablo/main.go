package MerkleStablo

import (
	"fmt"
	"log"
)

func main() {
	dataSet1 := []string{
		"Alice",
		"Bob",
		"Charlie",
		"David",
		"Eva",
		"Frank",
		"Grace",
		"Henry",
		"Ivy",
		"Jack",
	}
	merkleTree := NewMerkleTree(dataSet1)
	merkleTree.SerializeMerkleTree("merkletree.txt")

	dataSet2 := []string{
		"Alice",
		"Bobi", //
		"Charlie",
		"David",
		"Eva",
		"Franki", //
		"Grace",
		"Henry",
		"Ivy",
		"Jack",
	}

	mt, err := DeserializeMerkleTree("merkletree.txt")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("TEST CONTAINS")
	fmt.Println(mt.Contains("Bob"))  //true
	fmt.Println(mt.Contains("Bobi")) //false

	fmt.Println("\nTEST JEDNAKOSTI SETOVA")
	fmt.Println(mt.CheckMerkleTree(dataSet2))        //false
	fmt.Println(CheckTwoDataSet(dataSet1, dataSet2)) //false

	fmt.Println("\nTEST OSTECENIH ELEMENATA")
	fmt.Println(mt.DamagedElements(dataSet2)) //[Bobi, Franki]

}
