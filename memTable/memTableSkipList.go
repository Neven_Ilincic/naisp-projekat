package memTable

import (
	BT "BTree"
	EL "element"
	SL "skipList"
	"time"
)

type MemTableSkipList struct {
	MaxSize     int
	CurrentSize int
	SkipList    *SL.SkipList
	NumExisting int
	NumDeleted  int
	NumChanged  int
}

func (table *MemTableSkipList) NewMemTable(maxSize int) *MemTableSkipList {
	skipList := SL.NewSkipList(maxSize)
	return &MemTableSkipList{
		MaxSize:     maxSize,
		CurrentSize: 0,
		SkipList:    skipList,
		NumExisting: 0,
		NumDeleted:  0,
		NumChanged:  0,
	}
}

func (table *MemTableSkipList) AddElement(tumbstone bool, timestamp time.Time, key string, value string, edit bool) bool {
	if !edit {
		if table.CurrentSize >= table.MaxSize {
			return false
		}
	}

	element := EL.NewElement(tumbstone, timestamp, key, value)
	node := table.SkipList.Search(key)
	if node == nil {
		table.SkipList.Add(*element)
		table.CurrentSize += 1
		return true
	} else {
		if node.Element.Tumbstone == false {
			table.SkipList.Edit(key, value)

		} else {
			table.SkipList.Add(*element)
		}
		return true
	}

}

func (table *MemTableSkipList) DeleteElement(key string) bool {
	return table.SkipList.LogicDelete(key)
}

func (table *MemTableSkipList) EmptyMemTable() {
	skiplist := SL.NewSkipList(table.MaxSize)
	table.SkipList = skiplist
	table.CurrentSize = 0
	table.NumChanged = 0
	table.NumDeleted = 0
	table.NumExisting = 0
}

func (table *MemTableSkipList) IsFull() bool {
	if table.CurrentSize >= table.MaxSize {
		return true
	} else {
		return false
	}
}

func (table *MemTableSkipList) GetNumDeleted() int {
	return table.NumDeleted
}

func (table *MemTableSkipList) SetNumDeleted() {
	table.NumDeleted += 1
}

func (table *MemTableSkipList) GetNumExisting() int {
	return table.NumExisting
}

func (table *MemTableSkipList) SetNumExisting() {
	table.NumExisting += 1
}

func (table *MemTableSkipList) GetNumChanged() int {
	return table.NumChanged
}

func (table *MemTableSkipList) SetNumChanged() {
	table.NumChanged += 1
}

func (table *MemTableSkipList) GetSkipList() *SL.SkipList {
	return table.SkipList
}

func (table *MemTableSkipList) GetBTree() *BT.BTree {
	return nil
}
