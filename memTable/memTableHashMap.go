package memTable

import (
	EL "element"
	"sort"
	"time"
)

type ElementHM struct {
	Value     string
	Tumbstone bool
	Timestamp time.Time
}

func NewElement(tumbstone bool, timestamp time.Time, value string) *ElementHM {
	return &ElementHM{
		Value:     value,
		Tumbstone: tumbstone,
		Timestamp: timestamp,
	}
}

type MemTableHashMap struct {
	MaxSize     int
	CurrentSize int
	Table       *map[string]*ElementHM
	NumExisting int
	NumDeleted  int
	NumChanged  int
}

func (table *MemTableHashMap) NewMemTable(maxSize int) *MemTableHashMap {
	memTable := make(map[string]*ElementHM)
	return &MemTableHashMap{
		MaxSize:     maxSize,
		CurrentSize: 0,
		Table:       &memTable,
	}
}

func (table *MemTableHashMap) AddElement(tumbstone bool, timestamp time.Time, key string, value string, edit bool) bool {
	if !edit {
		if table.CurrentSize >= table.MaxSize {
			return false
		}
	}
	newElement := NewElement(tumbstone, timestamp, value)
	element, exists := (*(table.Table))[key]
	if !exists {
		//Dodavanje
		(*(table.Table))[key] = newElement
		table.CurrentSize += 1
		return true
	} else {
		//Izmena
		if element.Tumbstone == false {
			(*(table.Table))[key] = newElement
		} else {
			//Dodavanje na vec obrisan
			(*(table.Table))[key] = newElement
		}
		return true
	}
}

func (table *MemTableHashMap) DeleteElement(key string) bool {
	element, exists := (*(table.Table))[key]
	if exists {
		element.Tumbstone = true
		element.Timestamp = time.Now()
		(*(table.Table))[key] = element
		return true
	} else {
		return false
	}
}

func (table *MemTableHashMap) SortElements() {
	keys := make([]string, 0, len(*(table.Table)))
	for key := range *(table.Table) {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	sortedMap := make(map[string]*ElementHM)
	for _, key := range keys {
		sortedMap[key] = (*(table.Table))[key]
	}

	table.Table = &sortedMap
}

func (table *MemTableHashMap) IsFull() bool {
	if table.CurrentSize >= table.MaxSize {
		return true
	} else {
		return false
	}
}

func (table *MemTableHashMap) EmptyMemTable() {
	hashMap := make(map[string]*ElementHM)
	table.Table = &hashMap
	table.CurrentSize = 0
	table.NumChanged = 0
	table.NumDeleted = 0
	table.NumExisting = 0
}

func (table *MemTableHashMap) ReturnAllHashMap() []EL.Element {
	var listElement []EL.Element
	for key, elementHM := range *table.Table {
		if !elementHM.Tumbstone {
			element := EL.NewElement(elementHM.Tumbstone, elementHM.Timestamp, key, elementHM.Value)
			listElement = append(listElement, *element)
		}
	}
	return listElement
}

func (table *MemTableHashMap) GetNumDeleted() int {
	return table.NumDeleted
}

func (table *MemTableHashMap) SetNumDeleted() {
	table.NumDeleted += 1
}

func (table *MemTableHashMap) GetNumExisting() int {
	return table.NumExisting
}

func (table *MemTableHashMap) SetNumExisting() {
	table.NumExisting += 1
}

func (table *MemTableHashMap) GetNumChanged() int {
	return table.NumChanged
}

func (table *MemTableHashMap) SetNumChanged() {
	table.NumChanged += 1
}

func (table *MemTableHashMap) GetHashMap() *map[string]*ElementHM {
	return table.Table
}
