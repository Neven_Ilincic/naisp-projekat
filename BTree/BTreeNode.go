package BTree

import (
	"sort"
	"time"
)

type BTreeNode struct {
	Keys     []*BTreeKey
	Children []*BTreeNode
	Parent   *BTreeNode
	IsLeaf   bool
}

// Konstruktor cvora
func NewBTreeNode(keys []*BTreeKey) *BTreeNode {
	node := &BTreeNode{
		Keys:     keys,
		Children: nil,
		Parent:   nil,
		IsLeaf:   true,
	}

	return node
}

// Pretraga
func (node *BTreeNode) Search(key string) *BTreeNode {
	k := 0
	//Ako je index u opsegu radimo dalje sa njim
	for index := 0; index < len(node.Keys); index++ {
		if node.Keys[index].Key >= key {
			break
		} else {
			k += 1
		}
	}
	//Ako je taj index u opsegu i ako je to bas taj kljuc na tom indeksu vracamo pronadjeni cvor
	if k < len(node.Keys) {
		if node.Keys[k].Key == key && !node.Keys[k].Tumbstone {
			return node
		}
		//Ako je list a nismo nasli pre toga znaci da nema cvora sa tim kljucem
	} else if node.IsLeaf {
		return nil
		//Ako nije kraj rekurzivno pozovemo funkciju opet
	} else {
		return node.Children[k].Search(key)
	}
	return nil
}

// Pronalazi cvor u koji treba da semstim kljuc
func (node *BTreeNode) SearchInsert(key string) *BTreeNode {
	k := 0
	for index := 0; index < len(node.Keys); index++ {
		if node.Keys[index].Key >= key {
			break
		} else {
			k += 1
		}
	}

	if node.IsLeaf {
		return node
	} else {
		return node.Children[k].SearchInsert(key)
	}

}

// Pomocna funkcija koja vraca min broj kljuceva
func (node *BTreeNode) CheckMinKeys(m int) bool {
	if len(node.Keys) == (m - 1) {
		return true
	} else {
		return false
	}
}

// Pomocna funkcija koja vraca max broj kljuceva
func (node *BTreeNode) CheckMaxKeys(m int) bool {
	if len(node.Keys) == (2*m - 1) {
		return true
	} else {
		return false
	}
}

// Dodavanje kljuca
func (node *BTreeNode) AddKey(key string, value string) {
	bKey := NewBTreeKey(false, time.Now(), key, value)
	node.Keys = append(node.Keys, bKey)
	sort.Sort(ListKeys(node.Keys))
}

// Brisanje kljuca
func (node *BTreeNode) RemoveKey(key string) {
	for i, value := range node.Keys {
		if value.Key == key {
			//Nalazi trazeni kljuc i uklanja ga iz slice-a
			node.Keys = append(node.Keys[:i], node.Keys[i+1:]...)
			break
		}
	}

}

// Dodavanje deteta
func (node *BTreeNode) AddChild(other *BTreeNode) {

	listNodes := make([]*BTreeNode, 0)
	dodat := false
	for _, child := range node.Children {
		if len(child.Keys) != 0 {
			if child.Keys[0].Key > other.Keys[0].Key && !dodat {
				listNodes = append(listNodes, other)
				dodat = true
			}
			listNodes = append(listNodes, child)
		}

	}
	if !dodat {
		listNodes = append(listNodes, other)
	}
	node.Children = listNodes
	node.IsLeaf = false
	other.Parent = node

}

// Brisanje deteta
func (node *BTreeNode) RemoveChild(child *BTreeNode) {
	// Trazi index deteta koji uklanja
	index := -1
	for i, c := range node.Children {
		if c == child {
			index = i
			break
		}
	}
	//Ako ga ne pronadje, return
	if index == -1 {
		return
	}
	//Uklanjanje iz slice-a
	copy(node.Children[index:], node.Children[index+1:])
	node.Children = node.Children[:len(node.Children)-1]
}

// Pomocna funckija za pronalazenje srednjeg kljuca u nodu
func (node *BTreeNode) FindMedian() (string, string) {
	n := len(node.Keys)
	if n%2 == 1 {
		return node.Keys[n/2].Key, node.Keys[n/2].Value
	} else {
		return node.Keys[n/2-1].Key, node.Keys[n/2-1].Value
	}
}
