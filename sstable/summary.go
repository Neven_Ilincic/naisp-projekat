package sstable

import (
	"encoding/binary"
	"fmt"
	"os"
)

type SummaryEntry struct {
	Key    string
	Offset int64
}

func NewSummaryEntry(Key string, Offset int64) *SummaryEntry {
	return &SummaryEntry{
		Key:    Key,
		Offset: Offset,
	}
}

type Summary struct {
	FirstKey string
	LastKey  string
	FilePath string
	Entries  []*SummaryEntry
}

func NewSummary(FirstKey string, LastKey string, FilePath string) *Summary {
	return &Summary{
		FirstKey: FirstKey,
		LastKey:  LastKey,
		FilePath: FilePath,
		Entries:  make([]*SummaryEntry, 0),
	}
}

func (summary *Summary) AddEntry(key string, offset int64) {
	summary.Entries = append(summary.Entries, NewSummaryEntry(key, offset))

}

func (summary *Summary) Find(key string) (int64, string) {
	if key >= summary.FirstKey && key <= summary.LastKey {
		var offset int64
		offset = 0
		for _, summaryEntry := range summary.Entries {
			if summaryEntry.Key > key {
				return offset, summaryEntry.Key
			} else if summaryEntry.Key == key {
				return summaryEntry.Offset, summaryEntry.Key
			} else {
				offset = summaryEntry.Offset
			}
		}
		return offset, summary.LastKey
	} else {
		return -1, ""
	}
}

// ?
func (summary *Summary) WriteSummary() error {
	file, err := os.Create(summary.FilePath)
	if err != nil {
		return err
	}
	defer file.Close()

	// First key
	firstKeyBytes := []byte(summary.FirstKey)
	firstKeySize := len(firstKeyBytes)
	firstKeySize64 := int64(firstKeySize)
	bufferFirstKeySize := make([]byte, 8)
	binary.LittleEndian.PutUint64(bufferFirstKeySize, uint64(firstKeySize64))
	_, err = file.Write(bufferFirstKeySize)
	if err != nil {
		panic(err)
	}

	file.Write(firstKeyBytes)

	// Last key
	lastKeyBytes := []byte(summary.LastKey)
	lastKeySize := len(lastKeyBytes)
	lastKeySize64 := int64(lastKeySize)
	bufferLastKeySize := make([]byte, 8)
	binary.LittleEndian.PutUint64(bufferLastKeySize, uint64(lastKeySize64))
	_, err = file.Write(bufferLastKeySize)
	if err != nil {
		panic(err)
	}

	file.Write(lastKeyBytes)

	// Entries
	for _, entry := range summary.Entries {
		key := entry.Key
		keyBytes := []byte(key)
		keySize := len(keyBytes)
		keySize64 := int64(keySize)
		bufferKeySize := make([]byte, 8)
		binary.LittleEndian.PutUint64(bufferKeySize, uint64(keySize64))
		_, err = file.Write(bufferKeySize)
		if err != nil {
			panic(err)
		}

		file.Write(keyBytes)

		bufferOffsetSize := make([]byte, 8)
		binary.LittleEndian.PutUint64(bufferOffsetSize, uint64(entry.Offset))
		_, err = file.Write(bufferOffsetSize)
		if err != nil {
			panic(err)
		}
	}

	return nil
}

func (summary *Summary) LoadSummary() (*Summary, error) {
	file, err := os.Open(summary.FilePath)
	if err != nil {
		return summary, err
	}
	defer file.Close()

	summary.Entries = make([]*SummaryEntry, 0)

	firstKeySizeBuffer := make([]byte, 8)
	numRead1, err := file.Read(firstKeySizeBuffer)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return nil, err
	}

	if numRead1 != 8 {
		return nil, err
	}

	var firstKeySize int64
	for i := 0; i < len(firstKeySizeBuffer); i++ {
		firstKeySize |= int64(firstKeySizeBuffer[i]) << (8 * uint(i))
	}

	firstKeyBuffer := make([]byte, firstKeySize)
	numRead2, err := file.Read(firstKeyBuffer)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return nil, err
	}

	if numRead2 != int(firstKeySize) {
		return nil, err
	}

	var firstKeyString string
	for _, b := range firstKeyBuffer {
		firstKeyString += string(b)
	}

	summary.FirstKey = firstKeyString

	lastKeySizeBuffer := make([]byte, 8)
	numRead3, err := file.Read(lastKeySizeBuffer)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return nil, err
	}

	if numRead3 != 8 {
		return nil, err
	}

	var lastKeySize int64
	for i := 0; i < len(lastKeySizeBuffer); i++ {
		lastKeySize |= int64(lastKeySizeBuffer[i]) << (8 * uint(i))
	}

	lastKeyBuffer := make([]byte, lastKeySize)
	numRead4, err := file.Read(lastKeyBuffer)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return nil, err
	}

	if numRead4 != int(firstKeySize) {
		return nil, err
	}

	var lastKeyString string
	for _, b := range lastKeyBuffer {
		lastKeyString += string(b)
	}

	summary.LastKey = lastKeyString

	for {
		keySizeBuffer := make([]byte, 8)
		numReadA, err := file.Read(keySizeBuffer)
		if err != nil {
			if err.Error() == "EOF" {
				break // Reached end of file, exit the loop
			} else {
				fmt.Println("Error reading file:", err)
				return nil, err
			}
		}

		if numReadA != 8 {
			return nil, err
		}

		var keySize int64
		for i := 0; i < len(keySizeBuffer); i++ {
			keySize |= int64(keySizeBuffer[i]) << (8 * uint(i))
		}

		keyBuffer := make([]byte, keySize)
		numReadB, err := file.Read(keyBuffer)
		if err != nil {
			fmt.Println("Error reading file:", err)
			return nil, err
		}

		if numReadB != int(keySize) {
			return nil, err
		}

		var keyString string
		for _, b := range keyBuffer {
			keyString += string(b)
		}

		offsetBuffer := make([]byte, 8)
		numReadC, err := file.Read(offsetBuffer)
		if err != nil {
			if err.Error() == "EOF" {
				break // Reached end of file, exit the loop
			} else {
				fmt.Println("Error reading file:", err)
				return nil, err
			}
		}

		if numReadC != 8 {
			return nil, err
		}

		var offset int64
		for i := 0; i < len(offsetBuffer); i++ {
			offset |= int64(offsetBuffer[i]) << (8 * uint(i))
		}

		summary.AddEntry(keyString, offset)

	}

	return summary, nil
}
