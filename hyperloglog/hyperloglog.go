package hll

import (
	"crypto/md5"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"math"
	"math/bits"
	"os"
)

type HyperLogLog struct {
	M        int     // velicina seta
	P        int     // broj vodecih bitova koji se korsiti za bakete od 4 do 16
	Set      []int   //set
	Constant float64 //konstanta koja sluzi za dobijanje rezultata
}

func NewHyperLogLog(p int) *HyperLogLog {
	//p se prosledjuje mora biti izmedju 4 i 16 sto je vece manje su sanse za gresu ali i povecava velicinu seta
	if p < 4 || p > 16 {
		fmt.Println("Preciznost mora biti izmedju 4 i 16")
		return nil
	}
	m := math.Pow(2, float64(p))
	constant := getConstant(p)

	set := make([]int, int(m))
	for i := range set {
		set[i] = 0
	}

	return &HyperLogLog{
		M:        int(m),
		P:        p,
		Set:      set,
		Constant: constant,
	}
}

func FirstPBits(value int, p int) int {
	//pronalazi prvih p bitova i vraca vrednost
	extractedBits := value >> (32 - p)
	return extractedBits
}

func TrailingZeroBits(value int) int {
	//prebrojava broj uzastupnih nula od nazad
	return bits.TrailingZeros(uint(value))
}

func HashHLL(data []byte) int {
	//vraca hash vrednost uvek iste duzine
	hash := md5.Sum(data)
	return int(binary.BigEndian.Uint32(hash[:4]))
}

func (hll *HyperLogLog) Add(data []byte) {
	//nalazi hash vrednost vrednost prvih 5 bita i broj uzastupih nula + 1 na osnovu tih podataka upisuje vrednost u set
	hashValue := HashHLL(data)
	FirstPBits := FirstPBits(hashValue, hll.P)
	Value := TrailingZeroBits(hashValue) + 1
	if Value > hll.Set[FirstPBits] {
		hll.Set[FirstPBits] = Value
	}
}

func (hll *HyperLogLog) Estimate() float64 {
	//formula za predvidjanje
	res := hll.Constant * (float64(hll.M*hll.M) / hll.harmonicMean())
	return res
}

func (hll *HyperLogLog) harmonicMean() float64 {
	//formula za harmonisku sredinu koja se koristi u Estimate()
	var sum float64
	for _, value := range hll.Set {
		a := 1 << value
		sum += 1.0 / float64(a)
	}
	return sum
}

func getConstant(p int) float64 {
	//racunanje konstante u odnosu na p za Estimate()
	switch p {
	case 4:
		return 0.673
	case 5:
		return 0.697
	case 6:
		return 0.709
	default:
		return 0.7213 * (math.Pow(2, float64(p))) / ((math.Pow(2, float64(p))) + 1.079)
	}
}

func (hll *HyperLogLog) ResetToNeutral() {
	//vraca hll na pocetni polozaj
	for i := range hll.Set {
		hll.Set[i] = 0
	}
}

// Serijalizacija HLL strukture
func (hll *HyperLogLog) SerializeHyperLogLog(filePath string) error {
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer file.Close()

	data, err := json.Marshal(hll)
	if err != nil {
		return err
	}

	_, err = file.Write(data)
	if err != nil {
		return err
	}

	return nil
}

// Deserijalizacija HLL strukture
func DeserializeHyperLogLog(filePath string) (*HyperLogLog, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	var hyperLogLog HyperLogLog
	if err := decoder.Decode(&hyperLogLog); err != nil {
		return nil, err
	}

	return &hyperLogLog, nil
}
