package memTable

import (
	BT "BTree"
	EL "element"
	SL "skipList"
	"time"
)

type MemTableBTree struct {
	MaxSize     int
	CurrentSize int
	BTree       *BT.BTree
	NumExisting int
	NumDeleted  int
	NumChanged  int
}

func (table *MemTableBTree) NewMemTable(maxSize int) *MemTableBTree {
	btree := BT.NewBTree(BT.NewBTreeNode(make([]*BT.BTreeKey, 0)), 10, maxSize)
	return &MemTableBTree{
		MaxSize:     maxSize,
		CurrentSize: 0,
		BTree:       btree,
		NumExisting: 0,
		NumDeleted:  0,
		NumChanged:  0,
	}
}

func (table *MemTableBTree) AddElement(tumbstone bool, timestamp time.Time, key string, value string, edit bool) bool {
	if !edit {
		if table.CurrentSize >= table.MaxSize {
			return false
		}
	}

	if tumbstone != tumbstone || timestamp != timestamp {
		return false
	}

	node := table.BTree.Search(key)
	if node == nil {
		table.BTree.Insert(key, value)
		table.CurrentSize += 1
		return true
	} else {
		for _, bkey := range node.Keys {
			if bkey.Key == key {
				if bkey.Tumbstone == false {
					table.BTree.Edit(key, value)
				} else {
					table.BTree.Insert(key, value)
				}
				return true
			}
		}
		return false
	}

}

func (table *MemTableBTree) DeleteElement(key string) bool {
	return table.BTree.Delete(key)
}

func (table *MemTableBTree) EmptyMemTable() {
	btree := BT.NewBTree(BT.NewBTreeNode(make([]*BT.BTreeKey, 0)), 10, table.MaxSize)
	table.BTree = btree
	table.CurrentSize = 0
	table.NumChanged = 0
	table.NumDeleted = 0
	table.NumExisting = 0
}

func (table *MemTableBTree) IsFull() bool {
	if table.CurrentSize >= table.MaxSize {
		return true
	} else {
		return false
	}
}

func (table *MemTableBTree) ReturnAllValidBTree() []EL.Element {
	listElement := table.BTree.ReturnAllValid()
	return listElement
}

func (table *MemTableBTree) GetNumDeleted() int {
	return table.NumDeleted
}

func (table *MemTableBTree) SetNumDeleted() {
	table.NumDeleted += 1
}

func (table *MemTableBTree) GetNumExisting() int {
	return table.NumExisting
}

func (table *MemTableBTree) SetNumExisting() {
	table.NumExisting += 1
}

func (table *MemTableBTree) GetNumChanged() int {
	return table.NumChanged
}

func (table *MemTableBTree) SetNumChanged() {
	table.NumChanged += 1
}

func (table *MemTableBTree) GetSkipList() *SL.SkipList {
	return nil
}

func (table *MemTableBTree) GetBTree() *BT.BTree {
	return table.BTree
}
