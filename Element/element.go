package element

import "time"

type Element struct {
	Tumbstone bool
	Timestamp time.Time
	Key       string
	Value     string
}

func NewElement(tumbstone bool, timestamp time.Time, key string, value string) *Element {
	return &Element{
		Tumbstone: tumbstone,
		Timestamp: timestamp,
		Key:       key,
		Value:     value,
	}
}
