package SkipList

import (
	EL "element"
	"fmt"
	"math"
	"math/rand"
	"time"
)

type SkipListNode struct {
	Left    *SkipListNode
	Right   *SkipListNode
	Down    *SkipListNode
	Up      *SkipListNode
	Element EL.Element
}

func NewSkipListNode(element EL.Element) *SkipListNode {
	return &SkipListNode{
		Left:    nil,
		Right:   nil,
		Down:    nil,
		Up:      nil,
		Element: element,
	}
}

type SkipList struct {
	Head        *SkipListNode
	Height      int
	CurrentSize int
	MaxSize     int
}

func NewSkipList(maxSize int) *SkipList {
	if maxSize < 0 {
		return nil
	}
	skipListNode := NewSkipListNode(*EL.NewElement(false, time.Now(), "", ""))

	if maxSize == 0 {
		maxSize = math.MaxInt64
	}
	return &SkipList{
		Head:        skipListNode,
		Height:      1,
		CurrentSize: 0,
		MaxSize:     maxSize,
	}
}

// Poredjenje dva stringa
// Prvo se gledaju karakteri na istim pozicijama.
// Ako se naiđe na prvi karakter koji se razlikuje, string koji ima leksikografski veći karakter na toj poziciji smatra se većim.
// Ako su svi karakteri na istim pozicijama isti, tada se gleda dužina stringova, a duži string se smatra većim.
// Ako su i dužine iste, onda su stringovi jednaki.

func (SL *SkipList) Add(element EL.Element) {
	if SL.CurrentSize == SL.MaxSize {
		return
	}
	searchedNode := SL.Search(element.Key)
	//ako pokusamo da dodamo element sa kljucem koji je vec logicki obisan prvo obrisemo taj element fizicki pa ga zatim dodajemo
	if searchedNode != nil && searchedNode.Element.Tumbstone == true {
		SL.Delete(element.Key)
	}

	if searchedNode != nil && searchedNode.Element.Tumbstone == false {
		return
	}

	oldHeadNode := SL.Head //potreban za dodavanje u slucaju prazne liste za proveru jer se pre dodavanja uvek vrsi dodavanje potrebnih nivoa i head se menja

	//dodaje potrebne nivoe tj. dodaje head-eve na osnovu uzastupnih jedinica(c)
	c := roll()
	if c >= SL.Height {
		for i := c + 1 - SL.Height; i > 0; i-- {
			skipListNewHead := NewSkipListNode(*EL.NewElement(false, time.Now(), "", "")) //ovako izgleda head i njegovi down-i
			skipListNewHead.Down = SL.Head
			SL.Head.Up = skipListNewHead
			SL.Head = skipListNewHead
			SL.Height += 1
		}
	}

	//dodavanje u slucaju prazne liste vrsi se od head pa na dole
	if oldHeadNode.Down == nil && oldHeadNode.Right == nil {
		currentHead := SL.Head
		newNode := NewSkipListNode(element)
		newNode.Left = SL.Head
		SL.Head.Right = newNode
		for {
			if currentHead.Down != nil {
				newNode := NewSkipListNode(element)
				newNode.Up = currentHead.Right
				currentHead.Right.Down = newNode

				currentHead = currentHead.Down
				newNode.Left = currentHead
				currentHead.Right = newNode

			} else {
				break
			}

		}
		SL.CurrentSize += 1
		return
	}

	//currendHead postaje head u poslednjem nivoue
	//dobija se cvor posle kojeg se dodaje novi cvor
	currendHead := SL.Head
	for {
		if currendHead.Down != nil {
			currendHead = currendHead.Down
		} else {
			break
		}
	}

	compareNode := currendHead.Right
	for {

		if compareNode.Element.Key > element.Key {
			compareNode = compareNode.Left
			break
		} else {
			if compareNode.Right != nil {
				compareNode = compareNode.Right
			} else {
				break
			}

		}
	}
	//pravi se novi cvor i vezuje se sa potrebnim cvorovima samo u najnizem nivoue
	newNode := NewSkipListNode(element)
	newNode.Right = compareNode.Right
	newNode.Left = compareNode
	SL.CurrentSize += 1

	if compareNode.Right != nil {
		compareNode.Right.Left = newNode
	}
	compareNode.Right = newNode

	//dodavanje elementa na c broj visih nivoa
	for i := c; i > 0; i-- {
		newNodeNextLevel := NewSkipListNode(element)
		newNode.Up = newNodeNextLevel
		newNodeNextLevel.Down = newNode

		//leftNode i rightNode su prvi levi i desni cvorovi od novog cvora koji imaju vezu i sa gornjim nivoem
		leftNode := newNode.Left

		for {
			if leftNode == nil {
				leftNode = leftNode.Left
				continue
			}
			if leftNode != nil && leftNode.Up == nil {
				leftNode = leftNode.Left
			} else if leftNode != nil && leftNode.Up != nil {
				break
			}
		}

		var rightNode *SkipListNode
		if newNode.Right != nil {
			rightNode = newNode.Right
			for {
				if rightNode.Up == nil {
					if rightNode.Right != nil {
						rightNode = rightNode.Right
					} else {
						rightNode = nil
						break
					}
				} else {
					break
				}
			}
		} else {
			rightNode = nil
		}

		//leftNodeUp i rightNodeUp su cvorovi koji se vezuju za newNodeNextLevel odnoso cvor koji je dodat ali u visem nivou
		leftNodeUp := leftNode.Up
		var rightNodeUp *SkipListNode
		if rightNode != nil {
			rightNodeUp = rightNode.Up
		} else {
			rightNodeUp = nil
		}

		leftNodeUp.Right = newNodeNextLevel
		newNodeNextLevel.Left = leftNodeUp
		newNodeNextLevel.Element = element

		if rightNodeUp != nil {
			rightNodeUp.Left = newNodeNextLevel
			newNodeNextLevel.Right = rightNodeUp
		}

		//sad se sve desava na jednom sloju vise ukoliko for petlja ima sledecu iteraciju
		newNode = newNodeNextLevel

	}

}

func roll() int {
	//vraca broj koliko puta je dobijena uzastupna jedinica tj. na koliko jos nivoa ce se dodati vrednost
	result := 0
	for {
		c := rand.Intn(2)
		if c == 1 {
			result++
			continue
		} else {
			break
		}
	}
	return result
}

func (SL *SkipList) Delete(key string) {
	currentNode := SL.Search(key)
	if currentNode == nil {
		return
	}

	//u slucaju da je krajnja desna u jednom nivou uzima levu i njenu desnu stavlja na nil i tako i za sve iz viseg nivoa
	if currentNode.Right == nil {
		currentNode.Left.Right = nil
		for {
			if currentNode.Up != nil {
				currentNode = currentNode.Up
				currentNode.Left.Right = nil
			} else {
				break
			}
		}
		SL.CurrentSize -= 1
	}
	if currentNode.Right != nil {
		//u slucaju da nije krajnja desna u jednom nivou uzima levu i na njenu desnu postavlja desnu od trenutne
		//simetricno radi i za desnu, postupak se ponavlja i za istu vrednost na visim nivouima
		currentNode.Left.Right = currentNode.Right
		currentNode.Right.Left = currentNode.Left
		for {
			if currentNode.Up != nil {
				currentNode = currentNode.Up
				currentNode.Left.Right = currentNode.Right
				if currentNode.Right != nil {
					currentNode.Right.Left = currentNode.Left
				}
			} else {
				break
			}

		}
		SL.CurrentSize -= 1
	}

	//brise head i postavlja ga na jedan nivo ispod u slucaju da u najvisem nivou nema ni jednog elementa
	for {
		currentNode := SL.Head
		if currentNode.Right == nil {
			SL.Head = currentNode.Down
			currentNode.Down.Up = nil
			SL.Height -= 1

			currentNode = SL.Head
		} else {
			break
		}
	}

}

func (SL *SkipList) Search(key string) *SkipListNode {
	var result *SkipListNode
	currentNode := SL.Head

	//ako je lista prazna vraca prazan SkipListNode objekat
	if SL.IsEmpty() {
		return nil
	}

	//ako je trazena vrednos manja od trenutne ide desno, ukoliko je veca spusta se u nizi nivo, ako je jednaka vraca taj cvor
	//u slucaju da je sledeci desni veci a nema nizih slojeva element nije u listi
	//u slucaju da nema vise elemenata sa desne strane tj. svi su manji i pri tome nema nizih nivoa tada nema ni elementa u listi
	for {
		if currentNode.Right != nil && currentNode.Right.Element.Key == key {
			result = currentNode.Right
			break

		} else if currentNode.Right != nil && currentNode.Right.Element.Key < key {
			currentNode = currentNode.Right

		} else if currentNode.Right == nil && currentNode.Down == nil {
			return nil

		} else if currentNode.Right == nil || currentNode.Right.Element.Key > key {
			if currentNode.Down == nil {
				return nil
			}
			currentNode = currentNode.Down

		} else if currentNode.Right != nil && currentNode.Right.Element.Key > key && currentNode.Down == nil {
			return nil

		}

	}

	for {
		if result.Down != nil {
			result = result.Down
		} else {
			break
		}
	}

	return result

}

func (SL *SkipList) Edit(key, value string) {
	searchedNode := SL.Search(key)
	if searchedNode == nil {
		return
	}
	if searchedNode.Element.Tumbstone == true {
		return
	}
	searchedNode.Element.Value = value
	searchedNode.Element.Timestamp = time.Now()
	for {
		if searchedNode.Up != nil {
			searchedNode.Up.Element.Value = value
			searchedNode.Up.Element.Timestamp = time.Now()
			searchedNode = searchedNode.Up
		} else {
			break
		}
	}
}

func (SL *SkipList) LogicDelete(key string) bool {
	//Ako element ne postoji ili je vec obrisan nema sta da se radi
	searchedNode := SL.Search(key)
	if searchedNode == nil {
		return false
	}
	if searchedNode.Element.Tumbstone == true {
		return false
	}
	//Postavlja Tumbstone na true cime oznacava da je obrisan i menja time
	searchedNode.Element.Tumbstone = true
	searchedNode.Element.Timestamp = time.Now()

	//Radi isto i za sve cvorove koji su na visem novuo
	for {
		if searchedNode.Up != nil {
			searchedNode.Up.Element.Tumbstone = true
			searchedNode.Up.Element.Timestamp = time.Now()
			searchedNode = searchedNode.Up
		} else {
			break
		}
	}

	return true
}

func (SL *SkipList) ReturnAll() []EL.Element {
	headNode := SL.Head
	var listElement []EL.Element
	//Spusta se u head na najnizem nivou
	for {
		if headNode.Down != nil {
			headNode = headNode.Down
		} else {
			break
		}
	}
	for {
		if headNode.Right != nil {
			element := headNode.Right.Element
			//if !element.Tumbstone { //Ostaviti za svaki slucaj
			listElement = append(listElement, element)
			headNode = headNode.Right
			//}
		} else {
			break
		}
	}
	return listElement

}

func (SL *SkipList) IsEmpty() bool {
	//true ako je prazna
	if SL.CurrentSize == 0 {
		return true
	}
	return false
}

func PrintSkipList(skipList *SkipList) {
	fmt.Printf("SkipList Height: %d\n", skipList.Height)
	fmt.Printf("SkipList Current Size: %d\n", skipList.CurrentSize)
	fmt.Printf("SkipList Max Size: %d\n", skipList.MaxSize)

	currentNode := skipList.Head
	for currentNode != nil {
		PrintSkipListLevel(currentNode)
		currentNode = currentNode.Down
	}

	fmt.Println()
}

func PrintSkipListLevel(node *SkipListNode) {
	currentNode := node

	for currentNode != nil {
		fmt.Printf("%s -> ", currentNode.Element.Key)
		currentNode = currentNode.Right
	}

	fmt.Println("nil")
}
