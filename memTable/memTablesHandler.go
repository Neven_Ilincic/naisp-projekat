package memTable

import (
	LSM "lsm"
	"time"
	"utils"
)

type MemTablesHandler interface {
	// NewMemTablesHandler(numMemTables int, memTablesMaxSize int) *MemTablesHandler
	IsFullAllTables() bool
	FindFirstEmpty() int
	FindIndexContains(key string) (int, bool)
	EmptyFirst(utils.Config, int, int, *LSM.LSM) int          //
	LoadAllFromWal(utils.Config, int, *LSM.LSM) (int, string) //
	GetMemTablesSkipList() map[int]*MemTableSkipList
	GetMemTablesHashMap() map[int]*MemTableHashMap
	GetMemTablesBTree() map[int]*MemTableBTree
	WriteToMemtable(key string, value string, tombstone bool, timestamp time.Time, config utils.Config, lsm *LSM.LSM, p int)
}
