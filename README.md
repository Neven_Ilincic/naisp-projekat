# NAISP Projekat

## Naziv projekta
Key-Value engine

## Opis projekta
Projekat "Key-Value engine" implementiran u go jeziku služi za skladištenje podataka. Predstavlja konzolnu aplikaciju sa kojom korisnik može interagovati.
Osnovne operacije su: PUT(dodavanje), GET(dobavljanje), DELETE(brisanje)

## Test i pokretanje programa
Pokretanjem main file-a korisniku se nude opcije (1. PUT operacija, 2. DELETE operacija, 3. GET operacija, 0. IZLAZ) prikazane preko konzolnog menija. Korisnik može da bira jednu od opcija a ukoliko izabere opciju 0, program se završava. Korisnik može da doda željeni zapis u sistem, može da dobavi vrednost pod zadatim ključem  ili da obriše zapis pod zadatim ključem. Kako bi koristili željenu strukturu, potrebno ju je navesti u config file-u (moguće strukture: SkipList, B-Tree, HashMap). U suprotnom podrazumevajuća struktura biće SkipList.

## Autori
Autori projekta su: Mladen Petrić, Neven Ilinčić, Luka Vuković, Petar Dragičević.

## Svrha projekta
Projekat "Key-Value engine" napravljen je za potrebe predmeta "Napredni algoritmi i strukture podataka". 
