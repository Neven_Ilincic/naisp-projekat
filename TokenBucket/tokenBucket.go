package tokenBucket

import (
	"encoding/json"
	"os"
	"time"
)

type TokenBucket struct {
	Capacity     int
	Tokens       int
	LastRefill   time.Time
	RefillPeriod time.Duration
}

func NewTokenBucket(capacity int, refillPeriod time.Duration) *TokenBucket {
	return &TokenBucket{
		Capacity:     capacity,
		Tokens:       capacity,
		LastRefill:   time.Now(),
		RefillPeriod: refillPeriod,
	}
}

func (tb *TokenBucket) Refill() {
	now := time.Now()
	timeSinceLastRefill := now.Sub(tb.LastRefill)
	if timeSinceLastRefill >= tb.RefillPeriod {
		tb.Tokens = tb.Capacity //Obnovi sve tokene
		tb.LastRefill = now
	}
}

func (tb *TokenBucket) Consume(tokens int) bool {
	tb.Refill() //Obnovi tokene

	if tb.Tokens >= tokens {
		tb.Tokens -= tokens
		return true
	}
	return false
}
func (tb *TokenBucket) SerializeTokenBucket(filePath string) error {
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer file.Close()

	data, err := json.Marshal(tb)
	if err != nil {
		return err
	}

	_, err = file.Write(data)
	if err != nil {
		return err
	}

	return nil
}

func DeserializeTokenBucket(filePath string) (*TokenBucket, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	var tokenBucket TokenBucket
	if err := decoder.Decode(&tokenBucket); err != nil {
		return nil, err
	}

	return &tokenBucket, nil
}

func (tb *TokenBucket) SetCapacity(capacity int) {
	tb.Capacity = capacity
}

func (tb *TokenBucket) SetRefillPeriod(refillPeriodMinutes int) {
	var refillPeriod time.Duration
	refillPeriod = time.Duration(refillPeriodMinutes) * time.Minute
	tb.RefillPeriod = refillPeriod
}
