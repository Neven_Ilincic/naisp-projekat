package utils

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	WAL         WalConfig         `yaml:"WAL"`
	MemTable    MemTableConfig    `yaml:"MemTable"`
	Cache       CacheConfig       `yaml:"Cache"`
	Summary     SummaryConfig     `yaml:"Summary"`
	LSMTree     LSMTreeConfig     `yaml:"LSM Tree"`
	BloomFilter BloomFilterConfig `yaml:"Bloom Filter"`
	TokenBucket TokenBucketConfig `yaml:"Token Bucket"`
}

type WalConfig struct {
	MaxSegmentSize int `yaml:"Segment size"`
}

type MemTableConfig struct {
	NumMemTables int    `yaml:"Num MemTables"`
	MemTableSize int    `yaml:"Size"`
	Structure    string `yaml:"Structure"`
}

type CacheConfig struct {
	CacheSize int `yaml:"Size"`
}

type SummaryConfig struct {
	Thinning int `yaml:"Thinning"` //Proredjenost u summary
}

type LSMTreeConfig struct {
	MaxLevels   int `yaml:"Max Levels"`
	NumSSTables int `yaml:"Max SSTables per level"`
}

type BloomFilterConfig struct {
	FalsePositive float64 `yaml:"False-Positive"`
}

type TokenBucketConfig struct {
	Capacity            int `yaml:"Capacity"`
	RefillPeriodMinutes int `yaml:"Refill Period (Minutes)"`
}

// Zapisivanje config fajla u YAML formatu
func (config *Config) SaveConfig(filePath string) {
	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		panic("Greska prilikom zapisivanja config fajla!")
	}

	y, _ := yaml.Marshal(config)
	file.Write(y)
	file.Close()
}

// Citanje YAML config fajla
func ReadConfig(filePath string) *Config {
	file, err := os.ReadFile(filePath)
	if err != nil { // Postavljam na podrazumevane vrednosti
		fmt.Println("Greska prilikom ucitavanja config fajla, koristice se podrazumevane vrednosti!")
		config := SetDefaultConfig()
		return config
	}

	var readConfig Config
	err2 := yaml.Unmarshal(file, &readConfig)

	if err2 != nil {
		fmt.Println("Greska prilikom ucitavanja config fajla, koristice se podrazumevane vrednosti!")
		config := SetDefaultConfig()
		return config
	}

	config := &readConfig
	config.ValidateConfig()
	return config

}

// Provera ispravnosti config-a
func (config *Config) ValidateConfig() {
	if config.WAL.MaxSegmentSize <= 0 {
		config.WAL.SetSegmentSize(100)
	}
	if config.MemTable.NumMemTables <= 0 {
		config.MemTable.SetNumberMemTables(3)
	}
	if config.MemTable.MemTableSize <= 0 {
		config.MemTable.SetSize(5)
	}
	if config.MemTable.Structure != "SkipList" && config.MemTable.Structure != "B-Tree" && config.MemTable.Structure != "HashMap" {
		config.MemTable.SetStructure("SkipList")
	}
	if config.Cache.CacheSize <= 0 {
		config.Cache.SetSize(100)
	}
	if config.Summary.Thinning <= 0 {
		config.Summary.SetThinning(5)
	}
	if config.LSMTree.MaxLevels <= 0 {
		config.LSMTree.SetLevels(2)
	}
	if config.LSMTree.NumSSTables <= 0 {
		config.LSMTree.SetNumSSTables(4)
	}
	if config.BloomFilter.FalsePositive <= 0 {
		config.BloomFilter.SetFalsePositive(0.05)
	}
	if config.TokenBucket.Capacity <= 0 {
		config.TokenBucket.SetCapacity(10)
	}
	if config.TokenBucket.RefillPeriodMinutes <= 0 {
		config.TokenBucket.SetRefillPeriod(3)
	}
}

// Postavljanje na podrazumevane vrednosti
func SetDefaultConfig() *Config {
	walConfig := WalConfig{100}
	memtableConfig := MemTableConfig{3, 5, "SkipList"}
	cacheConfig := CacheConfig{100}
	summaryConfig := SummaryConfig{5}
	lmsTreeConfig := LSMTreeConfig{2, 4}
	bloomFilterConfig := BloomFilterConfig{0.05}
	tokenBucketConfig := TokenBucketConfig{10, 3}

	config := Config{walConfig, memtableConfig, cacheConfig, summaryConfig, lmsTreeConfig, bloomFilterConfig, tokenBucketConfig}
	return &config
}

// ///////// Geteri i seteri
// WAL
func (walConfig *WalConfig) SetSegmentSize(newSize int) {
	walConfig.MaxSegmentSize = newSize
}

func (walConfig *WalConfig) GetSegmentSize() int {
	return walConfig.MaxSegmentSize
}

// MemTable
func (memTableConfig *MemTableConfig) SetNumberMemTables(number int) {
	memTableConfig.NumMemTables = number
}

func (memTableConfig *MemTableConfig) GetNumberMemTables() int {
	return memTableConfig.NumMemTables
}

func (memTableConfig *MemTableConfig) SetSize(size int) {
	memTableConfig.MemTableSize = size
}

func (memTableConfig *MemTableConfig) GetSize() int {
	return memTableConfig.MemTableSize
}

func (memTableConfig *MemTableConfig) SetStructure(structure string) {
	memTableConfig.Structure = structure
}

func (memTableConfig *MemTableConfig) GetStructure() string {
	return memTableConfig.Structure
}

// Cache
func (cacheConfig *CacheConfig) SetSize(size int) {
	cacheConfig.CacheSize = size
}

func (cacheConfig *CacheConfig) GetSize() int {
	return cacheConfig.CacheSize
}

// Summary
func (summaryConfig *SummaryConfig) SetThinning(thinning int) {
	summaryConfig.Thinning = thinning
}

func (summaryConfig *SummaryConfig) GetThinning() int {
	return summaryConfig.Thinning
}

// LSM Stablo
func (lsmTreeConfig *LSMTreeConfig) SetLevels(levels int) {
	lsmTreeConfig.MaxLevels = levels
}

func (lsmTreeConfig *LSMTreeConfig) GetLevels() int {
	return lsmTreeConfig.MaxLevels
}

func (lsmTreeConfig *LSMTreeConfig) SetNumSSTables(numberSSTables int) {
	lsmTreeConfig.NumSSTables = numberSSTables
}

func (lsmTreeConfig *LSMTreeConfig) GetNumSSTables() int {
	return lsmTreeConfig.NumSSTables
}

// Bloom Filter
func (bloomFilterConfig *BloomFilterConfig) SetFalsePositive(falsePositive float64) {
	bloomFilterConfig.FalsePositive = falsePositive
}

func (bloomFilterConfig *BloomFilterConfig) GetFalsePositive() float64 {
	return bloomFilterConfig.FalsePositive
}

// Token Bucket
func (tokenBucketconfig *TokenBucketConfig) SetCapacity(capacity int) {
	tokenBucketconfig.Capacity = capacity
}
func (tokenBucketconfig *TokenBucketConfig) GetCapacity() int {
	return tokenBucketconfig.Capacity
}

func (tokenBucketconfig *TokenBucketConfig) SetRefillPeriod(period int) {
	tokenBucketconfig.RefillPeriodMinutes = period
}

func (tokenBucketconfig *TokenBucketConfig) GetRefillPeriod() int {
	return tokenBucketconfig.RefillPeriodMinutes
}
