package BTree

import "time"

type BTreeKey struct {
	Tumbstone bool
	Timestamp time.Time
	Key       string
	Value     string
}

//konstruktor
func NewBTreeKey(tumbstone bool, timestamp time.Time, key string, value string) *BTreeKey {
	return &BTreeKey{
		Tumbstone: tumbstone,
		Timestamp: timestamp,
		Key:       key,
		Value:     value,
	}
}

type ListKeys []*BTreeKey

func (p ListKeys) Len() int           { return len(p) }
func (p ListKeys) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p ListKeys) Less(i, j int) bool { return p[i].Key < p[j].Key }
