package memTable

import (
	BT "BTree"
	EL "element"
	SL "skipList"
	"time"
)

type MemTable interface {
	NewMemTable(maxSize int) *MemTable
	AddElement(tumbstone bool, timestamp time.Time, key string, value string, edit bool) bool
	DeleteElement(key string) bool
	IsFull() bool
	GetNumDeleted() int
	SetNumDeleted()
	GetNumExisting() int
	SetNumExisting()
	GetNumChanged() int
	SetNumChanged()
	GetSkipList() *SL.SkipList
	GetHashMap() *map[string]*ElementHM
	GetBTree() *BT.BTree
	ReturnAllHashMap() EL.Element
}
